CREATE USER 'auxenta'@'%' IDENTIFIED BY 'aux123';
GRANT ALL PRIVILEGES ON *.* TO 'auxenta'@'%';
FLUSH PRIVILEGES;

GRANT EXECUTE ON PROCEDURE mspaces_stg.averageRating TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_stg.bookingCharge TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_stg.filterSpace TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_stg.getCancelledDate TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_stg.isCancelledAfterPaid TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_stg.spaceByCoordinates TO 'auxenta'@'%';

GRANT EXECUTE ON PROCEDURE mspaces_preprod.averageRating TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_preprod.bookingCharge TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_preprod.filterSpace TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_preprod.getCancelledDate TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_preprod.isCancelledAfterPaid TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_preprod.spaceByCoordinates TO 'auxenta'@'%';


GRANT EXECUTE ON PROCEDURE mspaces_qae.averageRating TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_qae.bookingCharge TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_qae.filterSpace TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_qae.getCancelledDate TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_qae.isCancelledAfterPaid TO 'auxenta'@'%';
GRANT EXECUTE ON PROCEDURE mspaces_qae.spaceByCoordinates TO 'auxenta'@'%';

FLUSH PRIVILEGES;

SET GLOBAL time_zone = '+05:30';