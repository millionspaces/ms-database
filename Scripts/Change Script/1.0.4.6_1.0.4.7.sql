-- MySQL Workbench Synchronization
-- Generated: 2017-06-28 16:33
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `buffer_time` INT(11) NULL DEFAULT NULL AFTER `calendar_event_size`;

ALTER TABLE `eventspace`.`booking` 
ADD COLUMN `booking_charge` DOUBLE NULL DEFAULT NULL AFTER `event_type`;

CREATE TABLE IF NOT EXISTS `eventspace`.`availability_blocks` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `space` INT(11) NOT NULL,
  `from_time` TIME NULL DEFAULT NULL,
  `to_time` TIME NULL DEFAULT NULL,
  `weekday_charge` DOUBLE NULL DEFAULT NULL,
  `weekend_charge` DOUBLE NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT NULL,
  INDEX `fk_availablity_blocks_space1_idx` (`space` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_availablity_blocks_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.7');
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
