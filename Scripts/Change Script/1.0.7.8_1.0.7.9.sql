
-- MySQL Workbench Synchronization
-- Generated: 2018-01-05 10:39
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`space_type` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `icon` VARCHAR(45) NULL DEFAULT NULL,
  `mobile_icon` VARCHAR(45) NULL DEFAULT NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_space_type` (
  `space` INT(11) NOT NULL,
  `space_type` INT(11) NOT NULL,
  PRIMARY KEY (`space`, `space_type`),
  INDEX `fk_space_has_space_type_space_type1_idx` (`space_type` ASC),
  INDEX `fk_space_has_space_type_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_space_type_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_space_type_space_type1`
    FOREIGN KEY (`space_type`)
    REFERENCES `eventspace`.`space_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


ALTER TABLE `eventspace`.`space` 
DROP FOREIGN KEY `fk_space_reimbursable_options1`;

ALTER TABLE `eventspace`.`space` 
DROP INDEX `fk_space_reimbursable_options1_idx` ;

INSERT INTO `db_changes` (`version`) VALUES ('7.9');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
