-- MySQL Workbench Synchronization
-- Generated: 2018-01-08 15:30
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `space_additional_details` 
ADD COLUMN `contact_person_name2` VARCHAR(45) NULL DEFAULT NULL AFTER `company_phone`,
ADD COLUMN `mobile_phone2` VARCHAR(12) NULL DEFAULT NULL AFTER `contact_person_name2`,
ADD COLUMN `company_phone2` VARCHAR(12) NULL DEFAULT NULL AFTER `mobile_phone2`;


ALTER TABLE `mspaces_qae`.`promo_details` 
DROP COLUMN `space`;

CREATE TABLE IF NOT EXISTS `mspaces_qae`.`promo_details_has_space` (
  `promo_details` INT(11) NOT NULL,
  `space` INT(11) NOT NULL,
  PRIMARY KEY (`promo_details`, `space`),
  INDEX `fk_promo_details_has_space_space1_idx` (`space` ASC),
  INDEX `fk_promo_details_has_space_promo_details1_idx` (`promo_details` ASC),
  CONSTRAINT `fk_promo_details_has_space_promo_details1`
    FOREIGN KEY (`promo_details`)
    REFERENCES `mspaces_qae`.`promo_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_promo_details_has_space_space1`
    FOREIGN KEY (`space`)
    REFERENCES `mspaces_qae`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



INSERT INTO `db_changes` (`version`) VALUES ('8.1');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
