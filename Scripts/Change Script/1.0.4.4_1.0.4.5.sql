-- MySQL Workbench Synchronization
-- Generated: 2017-06-23 13:46
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `calendar_event_size` VARCHAR(45) NULL DEFAULT NULL AFTER `calendar_end`;

ALTER TABLE `eventspace`.`space_unavailablity` 
DROP COLUMN `space_rules`,
DROP COLUMN `seating_arrangement`,
DROP COLUMN `amenities`,
ADD COLUMN `date_booking_made` DATETIME NULL DEFAULT NULL AFTER `title`,
ADD COLUMN `guest_name` VARCHAR(245) NULL DEFAULT NULL AFTER `date_booking_made`,
ADD COLUMN `guest_contact_number` VARCHAR(45) NULL DEFAULT NULL AFTER `guest_name`,
ADD COLUMN `guest_email` VARCHAR(45) NULL DEFAULT NULL AFTER `guest_contact_number`,
ADD COLUMN `extras_requested` VARCHAR(545) NULL DEFAULT NULL AFTER `event_type_id`,
ADD COLUMN `seating_arrangement_id` INT(11) NOT NULL AFTER `extras_requested`,
ADD COLUMN `cost` VARCHAR(45) NULL DEFAULT NULL AFTER `seating_arrangement_id`,
ADD COLUMN `note` VARCHAR(545) NULL DEFAULT NULL AFTER `cost`,
ADD INDEX `fk_space_unavailablity_seating_arrangement1_idx` (`seating_arrangement_id` ASC);

ALTER TABLE `eventspace`.`space_unavailablity` 
ADD CONSTRAINT `fk_space_unavailablity_seating_arrangement1`
  FOREIGN KEY (`seating_arrangement_id`)
  REFERENCES `eventspace`.`seating_arrangement` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `eventspace`.`space_unavailablity` 
DROP FOREIGN KEY `fk_space_unavailablity_seating_arrangement1`;
ALTER TABLE `eventspace`.`space_unavailablity` 
CHANGE COLUMN `seating_arrangement_id` `seating_arrangement_id` INT(11) NULL ;
ALTER TABLE `eventspace`.`space_unavailablity` 
ADD CONSTRAINT `fk_space_unavailablity_seating_arrangement1`
  FOREIGN KEY (`seating_arrangement_id`)
  REFERENCES `eventspace`.`seating_arrangement` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
  INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.5');
  
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
