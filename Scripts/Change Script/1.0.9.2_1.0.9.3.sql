-- MySQL Workbench Synchronization
-- Generated: 2018-04-04 11:23
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


CREATE TABLE IF NOT EXISTS `external_api_code` (
  `id` INT(11) NOT NULL,
  `api_code` VARCHAR(10) NULL DEFAULT NULL,
  `root_url` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `api_details` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `external_api_code_has_api_details` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `external_api_code` INT(11) NOT NULL,
  `api_details` INT(11) NOT NULL,
  `api_request_method` INT(11) NOT NULL,
  `api_url` VARCHAR(45) NULL DEFAULT NULL,
  `external_space_id` INT NULL DEFAULT NULL, 
  PRIMARY KEY (`id`),
  INDEX `fk_external_api_code_has_api_details_api_details1_idx` (`api_details` ASC),
  INDEX `fk_external_api_code_has_api_details_external_api_code1_idx` (`external_api_code` ASC),
  INDEX `fk_external_api_code_has_api_details_api_request_method1_idx` (`api_request_method` ASC),
  CONSTRAINT `fk_external_api_code_has_api_details_external_api_code1`
    FOREIGN KEY (`external_api_code`)
    REFERENCES `external_api_code` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_external_api_code_has_api_details_api_details1`
    FOREIGN KEY (`api_details`)
    REFERENCES `api_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_external_api_code_has_api_details_api_request_method1`
    FOREIGN KEY (`api_request_method`)
    REFERENCES `api_request_method` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `api_request_method` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `space_external_apis` (
  `space` INT(11) NOT NULL,
  `external_api_code` INT(11) NOT NULL,
  PRIMARY KEY (`space`, `external_api_code`),
  INDEX `fk_space_external_apis_external_api_code1_idx` (`external_api_code` ASC),
  CONSTRAINT `fk_space_external_apis_space1`
    FOREIGN KEY (`space`)
    REFERENCES `space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_external_apis_external_api_code1`
    FOREIGN KEY (`external_api_code`)
    REFERENCES `external_api_code` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
  

INSERT INTO `db_changes` (`version`) VALUES ('9.3');  

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
