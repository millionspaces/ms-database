-- MySQL Workbench Synchronization
-- Generated: 2017-02-23 09:50
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`isCancelledAfterPaid`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `isCancelledAfterPaid`(in booking_id int)
BEGIN
	DECLARE b_id int(11) DEFAULT null;
    DECLARE b_status int(11) DEFAULT null;
    DECLARE is_cancelled_after_paid boolean default false;
    DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE cursor_name CURSOR FOR select booking,reservation_status from booking_history where booking=booking_id;
    
     -- declare NOT FOUND handler
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    IF EXISTS (SELECT booking from booking_history where booking=booking_id and reservation_status=4) THEN
		BEGIN
			OPEN cursor_name;
			get_list: LOOP
				FETCH cursor_name INTO b_id,b_status;
				IF v_finished = 1 THEN 
					LEAVE get_list;
				END IF;
				
				IF
					b_status=3
					THEN
						SET is_cancelled_after_paid=TRUE;
						SELECT is_cancelled_after_paid;
						LEAVE get_list;
				END IF;
			END LOOP get_list;
            SELECT is_cancelled_after_paid;
		END;
	END IF;
    SELECT is_cancelled_after_paid;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
