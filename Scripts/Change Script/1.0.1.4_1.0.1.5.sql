-- MySQL Workbench Synchronization
-- Generated: 2017-02-15 13:01
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`filterSpace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `filterSpace`(in params1 int,in params2 int,in rate1 double,in rate2 double,in amenityList varchar(500),in eventList varchar(500), in limitFor int)
BEGIN
	select dataset2.* from 	((
                select x.space as id,COALESCE(x.count_event,0) as b,COALESCE(y.count_amenity,0) as c from
                (select space, count(event_type) as count_event from space_has_event_type where event_type in (' + @eventList + ') group by space) as x
                left join
                (select space,count(amenity) as count_amenity from space_has_amenity where amenity in (' + @amenityList + ') group by space) as y
                on x.space=y.space)union
                (select y.space as id,COALESCE(x.count_event,0) as b,COALESCE(y.count_amenity,0) as c from
                (select space, count(event_type) as count_event from space_has_event_type where event_type in ('+@eventList+') group by space) as x
                right  join
                (select space, count(amenity) as count_amenity from space_has_amenity where amenity in (' + @amenityList + ') group by space) as y
                on x.space=y.space)) 	as dataset1	right join
                (select * from space WHERE participant_count>=params1 and participant_count<=params2 and rate_per_hour>=rate1 and rate_per_hour<=rate2) as dataset2
                on dataset1.id=dataset2.id 	group by dataset2.id
                order by dataset1.b desc,dataset1.c desc limit limitFor;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
