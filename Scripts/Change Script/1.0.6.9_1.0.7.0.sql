-- MySQL Workbench Synchronization
-- Generated: 2017-10-02 13:55
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`space_additional_details` (
  `space` INT NOT NULL,
  `contact_person_name` VARCHAR(45) NULL,
  `mobile_phone` VARCHAR(45) NULL,
  `company_phone` VARCHAR(45) NULL,
  `host_logo` VARCHAR(45) NULL,
  `account_holder_name` VARCHAR(45) NULL,
  `account_number` VARCHAR(45) NULL,
  `bank` VARCHAR(45) NULL,
  `bank_branch` VARCHAR(45) NULL,
  `commission_percentage` VARCHAR(45) NULL,
  `company_name` VARCHAR(45) NULL,
  INDEX `fk_table1_space1_idx` (`space` ASC),
  CONSTRAINT `fk_table1_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('7.0');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
