-- MySQL Workbench Synchronization
-- Generated: 2017-03-07 15:04
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`getCancelledDate`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCancelledDate`(in bookingId int, in bookingStatus int)
BEGIN
	select created_at from booking_history where booking=bookingId and reservation_status=bookingStatus;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
