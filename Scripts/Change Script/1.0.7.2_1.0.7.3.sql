-- MySQL Workbench Synchronization
-- Generated: 2017-10-19 13:38
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
CHANGE COLUMN `block_charge_type` `block_charge_type` VARCHAR(45) NULL DEFAULT NULL ,
ADD COLUMN `link_space` INT(11) NULL DEFAULT NULL AFTER `parent_id`,
ADD INDEX `fk_space_space2_idx` (`link_space` ASC);

ALTER TABLE `eventspace`.`space` 
ADD CONSTRAINT `fk_space_space2`
  FOREIGN KEY (`link_space`)
  REFERENCES `eventspace`.`space` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('7.3');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
