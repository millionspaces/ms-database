-- MySQL Workbench Synchronization
-- Generated: 2017-03-24 11:27
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`forceDeleteSpace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `forceDeleteSpace`(in  p_spaceId int(11))
begin
declare returnVal Boolean default 0;
IF EXISTS (select * from booking where space=p_spaceId) THEN
	BEGIN
		delete booking_has_extra_amenity
			from booking_has_extra_amenity
			where space_has_extra_amenity_space=p_spaceId;
        
        delete booking_history, reviews
        from booking
			left join booking_history
				on booking.id=booking_history.booking
			left join reviews
				on booking.id=reviews.booking_id
        where booking.space=p_spaceId;
        
        delete booking
			from booking
			where booking.space=p_spaceId;
       
	end;
    end if;
	delete image,space_has_amenity,space_has_event_type,space_has_extra_amenity
	from space
	left join image
		on space.id=image.space
	left join space_has_amenity
		on space.id=space_has_amenity.space
	left join space_has_event_type
		on space.id=space_has_event_type.space
	left join space_has_extra_amenity
		on space.id= space_has_extra_amenity.space
	where space.id=p_spaceId;
	
	delete space from space where space.id=p_spaceId;
	set returnVal = 1;
select returnVal;
end$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
