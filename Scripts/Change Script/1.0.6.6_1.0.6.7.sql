-- MySQL Workbench Synchronization
-- Generated: 2017-09-21 11:12
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`booking` 
ADD COLUMN `refund` DOUBLE NULL DEFAULT NULL AFTER `booking_charge`;


DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN

	IF NOT(New.reservation_status=OLD.reservation_status) then
    
	#if manual BOOKING
	IF New.reservation_status = 2 then
        if OLD.reservation_status =1 then
        	
		INSERT INTO `eventspace`.`booking_expire_details` (`booking`,`expire_date`) 
		VALUES (new.id,DATE_ADD(now(), INTERVAL 1 day));
            
		end if;
	END IF;

	#if manual pay
	IF New.reservation_status = 3 then
        if OLD.reservation_status =2 then
        
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
            
		end if;
	END IF;
    
		if New.reservation_status = 4 or New.reservation_status = 6 then
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
		end if;
		
		
		#if undo pay
		IF New.reservation_status = 2 then
			IF OLD.reservation_status =3 then
				UPDATE `eventspace`.`booking_expire_details`
				SET expired=0
				WHERE(booking= New.id);
            
				DELETE FROM `eventspace`.`booking_history` 
				WHERE  booking=new.id and reservation_status=OLD.reservation_status;
			END IF;
			IF OLD.reservation_status =1 then	
				INSERT INTO `eventspace`.`booking_history`
				VALUES(New.id, New.reservation_status, NULL, New.action_taker, now(), 0);
				END IF;
        END IF;
		
		
			IF NOT (New.reservation_status = 2)  then
				INSERT INTO `eventspace`.`booking_history`
				VALUES(New.id, New.reservation_status, New.refund, New.action_taker, now(), 0);
			END IF;
	END IF;
END$$


DELIMITER ;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('6.7');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
