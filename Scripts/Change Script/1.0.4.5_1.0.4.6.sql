-- MySQL Workbench Synchronization
-- Generated: 2017-06-12 15:27
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_INSERT` $$

USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,null,New.action_taker,now(),0);
    
    # booking_expire_details
	IF New.reservation_status = 2 then 
		INSERT INTO `eventspace`.`booking_expire_details` (`booking`,`expire_date`) 
		VALUES (new.id,DATE_ADD(now(), INTERVAL 1 day));
	END IF;
END$$


DELIMITER ;

DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN

	# pay
	IF New.reservation_status = 3 then
        if OLD.reservation_status =2 then
        
			#if manual pay
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
            
            #if pay after undo pay
            if exists(select id from `eventspace`.`space_unavailablity`
            where space = new.space and from_date= new.from_date and to_date= new.to_date)
			then
				UPDATE `eventspace`.`space_unavailablity`
				SET is_blocked=1
				WHERE(space = new.space and from_date= new.from_date and to_date= new.to_date);
            else
			#if manual first pay
				INSERT INTO `eventspace`.`space_unavailablity`(`space`,`from_date`,`to_date`,`is_blocked`,`seating_arrangement_id`) 
				VALUES(new.space, new.from_date, new.to_date,1,1);
            end if;
       else
		#if ipg pay
			INSERT INTO `eventspace`.`space_unavailablity`(`space`,`from_date`,`to_date`,`is_blocked`,`seating_arrangement_id`) 
			VALUES(new.space, new.from_date, new.to_date,1,1);
		end if;
	END IF;
    
		#if undo pay
		IF New.reservation_status = 2 then
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=0
			WHERE(booking= New.id);
            
            UPDATE `eventspace`.`space_unavailablity`
			SET is_blocked=0
			WHERE(space = new.space and from_date= new.from_date and to_date= new.to_date);
            
            DELETE FROM `eventspace`.`booking_history` 
            WHERE  booking=new.id and reservation_status=OLD.reservation_status;
		ELSE 
		# not undo pay
			INSERT INTO `eventspace`.`booking_history`
			VALUES(New.id, New.reservation_status, NULL, New.action_taker, now(), 0);
		END IF;
        
        if New.reservation_status = 4 or New.reservation_status = 6 then
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
		end if;
END$$


DELIMITER ;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.6');
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
