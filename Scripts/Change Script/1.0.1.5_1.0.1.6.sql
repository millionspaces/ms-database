-- MySQL Workbench Synchronization
-- Generated: 2017-02-15 14:44
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`averageRating`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `averageRating`(in spaceId int)
BEGIN
	SELECT round(avg(r.rate),2) as average 
    FROM booking b, reviews r, space s 
    where b.id=r.booking_id 
    and s.id=b.space 
    and s.id=spaceId;
END$$

DELIMITER ;

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`spaceByCoordinates`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spaceByCoordinates`(in latitudeVal varchar(50),in longitudeVal varchar(50),in radius int)
BEGIN
	SELECT * 
    from space 
    where ( 6371 * acos( cos( radians(latitudeVal) ) * 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - radians(longitudeVal) ) + 
    sin( radians(latitudeVal) ) * 
    sin( radians( latitude ) ) ) )  < radius;

END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
