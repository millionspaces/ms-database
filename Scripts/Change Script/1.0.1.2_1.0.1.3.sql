-- MySQL Workbench Synchronization
-- Generated: 2017-02-09 15:17
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`booking` 
DROP FOREIGN KEY `fk_booking_event_sub_type1`;

ALTER TABLE `eventspace`.`booking` 
DROP COLUMN `event_sub_type`,
ADD COLUMN `event_type` INT(11) NOT NULL AFTER `action_taker`,
ADD INDEX `fk_booking_event_type1_idx` (`event_type` ASC),
DROP INDEX `fk_booking_event_sub_type1_idx` ;

DROP TABLE IF EXISTS `eventspace`.`event_sub_type` ;

ALTER TABLE `eventspace`.`booking` 
ADD CONSTRAINT `fk_booking_event_type1`
  FOREIGN KEY (`event_type`)
  REFERENCES `eventspace`.`event_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
