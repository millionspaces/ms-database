-- MySQL Workbench Synchronization
-- Generated: 2017-10-02 10:42
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`block_charge_type` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


ALTER TABLE `eventspace`.`space` 
CHANGE COLUMN `min_participant_count` `min_participant_count` INT(11) NULL DEFAULT NULL AFTER `charge_type`,
ADD COLUMN `block_charge_type` VARCHAR(45) NULL DEFAULT 1 AFTER `min_participant_count`;

ALTER TABLE `eventspace`.`block_availability` 
ADD COLUMN `is_reimbursable` TINYINT(1) NULL DEFAULT 0 AFTER `active`;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('6.9');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
