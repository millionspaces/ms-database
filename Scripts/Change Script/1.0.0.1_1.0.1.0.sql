SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`booking` 
ADD COLUMN `action_taker` INT(11) NOT NULL AFTER `to_date`;


CREATE TABLE IF NOT EXISTS `eventspace`.`booking_history` (
  `booking` INT(11) NOT NULL,
  `reservation_status` INT(11) NOT NULL,
  `action_taker` INT(11) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,

  INDEX `fk_booking_history_booking1_idx` (`booking` ASC),
  INDEX `fk_booking_history_reservation_status1_idx` (`reservation_status` ASC),
  CONSTRAINT `fk_booking_history_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_history_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_history_user1`
	FOREIGN KEY (`action_taker`)
	REFERENCES `eventspace`.`user` (`id`)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


USE `eventspace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,New.action_taker,now());
END$$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,New.action_taker,now());
END$$


DELIMITER ;