-- MySQL Workbench Synchronization
-- Generated: 2017-09-07 12:37
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `reimbursable_options` INT(11) NULL DEFAULT NULL AFTER `menu_file_name`,
ADD INDEX `fk_space_reimbursable_options1_idx` (`reimbursable_options` ASC);

CREATE TABLE IF NOT EXISTS `eventspace`.`menu_files` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_menu_files` (
  `space` INT(11) NOT NULL,
  `menu_files` INT(11) NOT NULL,
  PRIMARY KEY (`space`, `menu_files`),
  INDEX `fk_space_has_menu_files_menu_files1_idx` (`menu_files` ASC),
  INDEX `fk_space_has_menu_files_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_menu_files_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_menu_files_menu_files1`
    FOREIGN KEY (`menu_files`)
    REFERENCES `eventspace`.`menu_files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`block_availability_has_menu_files` (
  `block_availability` INT(11) NOT NULL,
  `menu_files` INT(11) NOT NULL,
  PRIMARY KEY (`block_availability`, `menu_files`),
  INDEX `fk_block_availability_has_menu_files_menu_files1_idx` (`menu_files` ASC),
  INDEX `fk_block_availability_has_menu_files_block_availability1_idx` (`block_availability` ASC),
  CONSTRAINT `fk_block_availability_has_menu_files_block_availability1`
    FOREIGN KEY (`block_availability`)
    REFERENCES `eventspace`.`block_availability` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_block_availability_has_menu_files_menu_files1`
    FOREIGN KEY (`menu_files`)
    REFERENCES `eventspace`.`menu_files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`reimbursable_options` (
  `id` INT(11) NOT NULL,
  `code` VARCHAR(45) NULL DEFAULT NULL,
  `desc` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `eventspace`.`space` 
ADD CONSTRAINT `fk_space_reimbursable_options1`
  FOREIGN KEY (`reimbursable_options`)
  REFERENCES `eventspace`.`reimbursable_options` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

  
INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('6.3');
