-- MySQL Workbench Synchronization
-- Generated: 2017-12-20 10:13
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS`payment_verify` (
  `booking_id` INT(11) NOT NULL,
  `verified` INT(11) NOT NULL DEFAULT 0,
  INDEX `fk_payment_verify_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_payment_verify_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `db_changes` (`version`) VALUES ('7.8');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
