-- MySQL Workbench Synchronization
-- Generated: 2017-07-11 16:55
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
DROP FOREIGN KEY `fk_space_measurement_unit1`;

ALTER TABLE `eventspace`.`space` 
DROP COLUMN `measurement_unit`,
ADD COLUMN `measurement_unit` INT(11) NOT NULL AFTER `notice_period`,
ADD COLUMN `charge_type` INT(11) NOT NULL AFTER `measurement_unit`,
ADD INDEX `fk_space_measurement_unit1_idx` (`measurement_unit` ASC),
ADD INDEX `fk_space_charge_type1_idx` (`charge_type` ASC),
DROP INDEX `fk_space_measurement_unit1_idx` ;

CREATE TABLE IF NOT EXISTS `eventspace`.`per_hour_availablity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `space` INT(11) NOT NULL,
  `day` INT(11) NOT NULL,
  `from_time` TIME NOT NULL,
  `to_time` TIME NOT NULL,
  `charge` DOUBLE NOT NULL,
  `active` SMALLINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_per_hour_availablity_space1_idx` (`space` ASC),
  INDEX `fk_per_hour_availablity_day1_idx` (`day` ASC),
  CONSTRAINT `fk_per_hour_availablity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_per_hour_availablity_day1`
    FOREIGN KEY (`day`)
    REFERENCES `eventspace`.`day` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`charge_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`day` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `eventspace`.`space` 
ADD CONSTRAINT `fk_space_measurement_unit1`
  FOREIGN KEY (`measurement_unit`)
  REFERENCES `eventspace`.`measurement_unit` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_space_charge_type1`
  FOREIGN KEY (`charge_type`)
  REFERENCES `eventspace`.`charge_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



ALTER TABLE `eventspace`.`availability_blocks` 
DROP COLUMN `weekend_charge`,
CHANGE COLUMN `weekday_charge` `charge` DOUBLE NULL DEFAULT NULL ,
ADD COLUMN `day` INT(11) NOT NULL DEFAULT 1 AFTER `space`,
ADD INDEX `fk_block_availability_day1_idx` (`day` ASC), RENAME TO  `eventspace`.`block_availability` ;

ALTER TABLE `eventspace`.`block_availability` 
ADD CONSTRAINT `fk_block_availability_day1`
  FOREIGN KEY (`day`)
  REFERENCES `eventspace`.`day` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('5.1');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
