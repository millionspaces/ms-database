-- MySQL Workbench Synchronization
-- Generated: ,018-03-07 1,:4,
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `space_contact_persons` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `space` INT(11) NOT NULL,
  `contact_person_name` VARCHAR(45) NULL DEFAULT NULL,
  `contact_person_email` VARCHAR(45) NULL DEFAULT NULL,
  `contact_person_mobile` VARCHAR(10) NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT 1,
  INDEX `fk_space_contact_persons_space1_idx` (`space` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_space_contact_persons_space1`
    FOREIGN KEY (`space`)
    REFERENCES `space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;




INSERT INTO `db_changes` (`version`) VALUES ('9.1');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
