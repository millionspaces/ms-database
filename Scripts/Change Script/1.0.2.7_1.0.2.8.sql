-- MySQL Workbench Synchronization
-- Generated: 2017-03-15 10:38
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`booking_history` 
ADD COLUMN `seen` TINYINT(1) NOT NULL DEFAULT 0 AFTER `created_at`;


DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_INSERT` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,null,New.action_taker,now(),0);
END$$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	declare isPaid int;
    declare refund decimal;
	declare duration int;
    declare cancellationPolicy int; 
    
    set isPaid=0;
    if exists(SELECT booking from booking_history where booking=New.id and reservation_status=3) 
    then
    if New.reservation_status=4
    then
    set isPaid=1;
    end if;
    end if;
    
    if isPaid=1 then
	set cancellationPolicy=(select s.cancellation_policy from booking b,space s where s.id=b.space and b.id=New.id); 
    set duration=(select TIMESTAMPDIFF(hour,now(),from_date)  from booking  where id=New.id); 
		if cancellationPolicy =3  then if duration < 168 then set refund=0; end if;end if;
		if cancellationPolicy =2  then if duration < 120 then set refund=0; end if;end if;
        if cancellationPolicy =1  then if duration < 48 then set refund=0; end if;end if;
		if (refund is null) then  
			set refund=(select(round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date)/3600,2)
				*space.rate_per_hour+
				COALESCE(sum(booking_has_extra_amenity.number*booking_has_extra_amenity.rate),0))*refund_rate
				FROM booking 
				join space 
				on booking.space=space.id 
				left join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking
                join cancellation_policy
                on cancellation_policy.id=space.cancellation_policy
				where booking.id=New.id);
			end if;   
     end if;  
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,refund,New.action_taker,now(),0);
END$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
