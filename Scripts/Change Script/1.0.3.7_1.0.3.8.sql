-- MySQL Workbench Synchronization
-- Generated: 2017-06-02 10:37
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`payment_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `booking` INT(11) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_history_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_payment_history_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`bookingCharge`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `bookingCharge`(in bookingId int)
BEGIN
	SELECT (round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date)/3600,2)
				*space.rate_per_hour+
				COALESCE(sum(booking_has_extra_amenity.number*booking_has_extra_amenity.rate),0))*1
				FROM booking 
				join space 
				on booking.space=space.id 
				left join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking
				where booking.id=bookingId;
END$$

DELIMITER ;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('3.8');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
