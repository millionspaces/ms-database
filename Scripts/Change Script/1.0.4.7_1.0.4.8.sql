-- MySQL Workbench Synchronization
-- Generated: 2017-06-28 16:33
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space_unavailablity` 
DROP FOREIGN KEY `fk_space_unavailablity_seating_arrangement1`;
ALTER TABLE `eventspace`.`space_unavailablity` 
CHANGE COLUMN `seating_arrangement_id` `seating_arrangement_id` INT(11) NULL ;
ALTER TABLE `eventspace`.`space_unavailablity` 
ADD CONSTRAINT `fk_space_unavailablity_seating_arrangement1`
  FOREIGN KEY (`seating_arrangement_id`)
  REFERENCES `eventspace`.`seating_arrangement` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.8');
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
