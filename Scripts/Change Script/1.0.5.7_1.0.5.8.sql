-- MySQL Workbench Synchronization
-- Generated: 2017-08-01 17:34
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`amenity` 
ADD COLUMN `mobile_icon` VARCHAR(45) NULL DEFAULT NULL AFTER `icon`;

ALTER TABLE `eventspace`.`event_type` 
ADD COLUMN `mobile_icon` VARCHAR(45) NULL DEFAULT NULL AFTER `icon`;

ALTER TABLE `eventspace`.`seating_arrangement` 
ADD COLUMN `mobile_icon` VARCHAR(45) NULL DEFAULT NULL AFTER `icon`;

ALTER TABLE `eventspace`.`rule` 
ADD COLUMN `mobile_icon` VARCHAR(45) NULL DEFAULT NULL AFTER `icon`;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('5.8');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
