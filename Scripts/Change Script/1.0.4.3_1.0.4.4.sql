-- MySQL Workbench Synchronization
-- Generated: 2017-06-22 12:08
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `calendar_start` DATE NULL DEFAULT NULL AFTER `approved`,
ADD COLUMN `calendar_end` DATE NULL DEFAULT NULL AFTER `calendar_start`;

ALTER TABLE `eventspace`.`space_unavailablity` 
ADD COLUMN `title` VARCHAR(245) NULL DEFAULT NULL ,
ADD COLUMN `amenities` VARCHAR(445) NULL DEFAULT NULL AFTER `title`,
ADD COLUMN `no_of_guests` VARCHAR(445) NULL DEFAULT NULL AFTER `amenities`,
ADD COLUMN `seating_arrangement` VARCHAR(445) NULL DEFAULT NULL AFTER `no_of_guests`,
ADD COLUMN `space_rules` VARCHAR(445) NULL DEFAULT NULL AFTER `seating_arrangement`,
ADD COLUMN `event_type_id` INT(11) NULL DEFAULT NULL AFTER `space_rules`,
ADD INDEX `fk_space_unavailablity_event_type1_idx` (`event_type_id` ASC);

ALTER TABLE `eventspace`.`space_unavailablity` 
ADD CONSTRAINT `fk_space_unavailablity_event_type1`
  FOREIGN KEY (`event_type_id`)
  REFERENCES `eventspace`.`event_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


  INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.4');
  
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
