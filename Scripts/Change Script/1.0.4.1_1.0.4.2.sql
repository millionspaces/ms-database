-- MySQL Workbench Synchronization
-- Generated: 2017-06-14 17:21
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`user` 
ADD COLUMN `company_phone` VARCHAR(45) NULL DEFAULT NULL AFTER `phone`;

ALTER TABLE `eventspace`.`space` 
CHANGE COLUMN `approved` `approved` TINYINT(1) NOT NULL ,
ADD COLUMN `security_deposit` INT(11) NULL DEFAULT NULL AFTER `thumbnail_image`;

ALTER TABLE `eventspace`.`space_has_extra_amenity` 
ADD COLUMN `amenity_unit` INT(11) NOT NULL  AFTER `amenity`,
ADD INDEX `fk_space_has_extra_amenity_amenity_unit1_idx` (`amenity_unit` ASC);

CREATE TABLE IF NOT EXISTS `eventspace`.`seating_arrangement` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NULL DEFAULT NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`auto_publish` (
  `id` INT(11) NOT NULL,
  `enabled` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_seating_arrangement` (
  `space` INT(11) NOT NULL,
  `seating_arrangement` INT(11) NOT NULL,
  `participant_count` INT(11) NOT NULL,
  INDEX `fk_space_has_space1_idx` (`space` ASC),
  INDEX `fk_space_has_seating_arrangement_seating_arrangement1_idx` (`seating_arrangement` ASC),
  PRIMARY KEY (`space`, `seating_arrangement`),
  CONSTRAINT `fk_space_has_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_seating_arrangement_seating_arrangement1`
    FOREIGN KEY (`seating_arrangement`)
    REFERENCES `eventspace`.`seating_arrangement` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`rule` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NULL DEFAULT NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_rule` (
  `space` INT(11) NOT NULL,
  `rule` INT(11) NOT NULL,
  INDEX `fk_space_has_rule_space1_idx` (`space` ASC),
  INDEX `fk_space_has_rule_rules1_idx` (`rule` ASC),
  PRIMARY KEY (`space`, `rule`),
  CONSTRAINT `fk_space_has_rule_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_rule_rules1`
    FOREIGN KEY (`rule`)
    REFERENCES `eventspace`.`rule` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `eventspace`.`space_has_extra_amenity` 
ADD CONSTRAINT `fk_space_has_extra_amenity_amenity_unit1`
  FOREIGN KEY (`amenity_unit`)
  REFERENCES `eventspace`.`amenity_unit` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
  DELIMITER $$
USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`space_BEFORE_INSERT` BEFORE INSERT ON `space` FOR EACH ROW
BEGIN
   declare approve int;
    if exists(select enabled from auto_publish where id =1) 
		then
		set approve= (select enabled from auto_publish where id =1);
		SET NEW.approved = approve;
	end if;
END$$
DELIMITER ;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.2');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
