-- MySQL Workbench Synchronization
-- Generated: 2018-04-23 12:49
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `primary_event_type` INT(11) NULL DEFAULT NULL AFTER `approved_date`,
ADD INDEX `fk_space_event_type1_idx` (`primary_event_type` ASC);
ALTER TABLE `eventspace`.`space` 
ADD CONSTRAINT `fk_space_event_type1`
  FOREIGN KEY (`primary_event_type`)
  REFERENCES `eventspace`.`event_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


INSERT INTO `db_changes` (`version`) VALUES ('1.0.0');  

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
