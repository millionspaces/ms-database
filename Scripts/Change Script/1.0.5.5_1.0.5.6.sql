-- MySQL Workbench Synchronization
-- Generated: 2017-07-26 10:10
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`booking` 
DROP COLUMN `to_date`,
DROP COLUMN `from_date`;

CREATE TABLE IF NOT EXISTS `eventspace`.`booking_slots` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking` INT NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  INDEX `fk_booking_slots_booking1_idx` (`booking` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_booking_slots_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN

	# pay
	IF New.reservation_status = 3 then
        if OLD.reservation_status =2 then
        
			#if manual pay
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
            
		end if;
	END IF;
    
		#if undo pay
		IF New.reservation_status = 2 then
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=0
			WHERE(booking= New.id);
            
            DELETE FROM `eventspace`.`booking_history` 
            WHERE  booking=new.id and reservation_status=OLD.reservation_status;
		ELSE 
		# not undo pay
			INSERT INTO `eventspace`.`booking_history`
			VALUES(New.id, New.reservation_status, NULL, New.action_taker, now(), 0);
		END IF;
        
        if New.reservation_status = 4 or New.reservation_status = 6 then
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
		end if;
END$$


DELIMITER ;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('5.6');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
