-- MySQL Workbench Synchronization
-- Generated: 2017-10-12 10:49
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`promo_details` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `promo_code` VARCHAR(45) NOT NULL,
  `user` INT(11) NOT NULL,
  `discount` INT(11) NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_promo_details_user1_idx` (`user` ASC),
  UNIQUE INDEX `promo_code_UNIQUE` (`promo_code` ASC),
  CONSTRAINT `fk_promo_details_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`booking_has_promotion` (
  `booking` INT(11) NOT NULL,
  `promo_details` INT(11) NOT NULL,
  INDEX `fk_booking_has_promotion_booking1_idx` (`booking` ASC),
  INDEX `fk_booking_has_promotion_promo_details1_idx` (`promo_details` ASC),
  CONSTRAINT `fk_booking_has_promotion_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_has_promotion_promo_details1`
    FOREIGN KEY (`promo_details`)
    REFERENCES `eventspace`.`promo_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('7.1');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
