-- MySQL Workbench Synchronization
-- Generated: 2017-02-13 11:17
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`deleteSpace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteSpace`(in  p_spaceId int(11))
begin
declare returnVal Boolean default 0;
IF NOT EXISTS (select * from booking where space=p_spaceId) THEN
	BEGIN
		delete image,space_has_extra_amenity,space_has_amenity,space_has_event_type
		from space
		join image
			on space.id=image.space
		join space_has_amenity
			on space.id=space_has_amenity.space
		join space_has_event_type
			on space.id=space_has_event_type.space
		join space_has_extra_amenity
			on space.id=space_has_extra_amenity.space
		where space.id=p_spaceId;
		
		delete space from space where space.id=p_spaceId;
        set returnVal = 1;
	END;

end if;
select returnVal;
end$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
