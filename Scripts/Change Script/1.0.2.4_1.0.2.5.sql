-- MySQL Workbench Synchronization
-- Generated: 2017-03-08 16:34
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

USE `eventspace`;
ALTER TABLE `eventspace`.`reviews` 
CHANGE COLUMN `rate` `rate` VARCHAR(10) NOT NULL ;

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`averageRating`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `averageRating`(in spaceId int)
BEGIN
	SELECT round(avg(SUBSTRING_INDEX(r.rate,'|',1)),2) as average 
    FROM booking b, reviews r, space s 
    where b.id=r.booking_id 
    and s.id=b.space 
    and s.id=spaceId;
END$$
DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
