-- MySQL Workbench Synchronization
-- Generated: 2017-03-01 23:10
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`cancellation_policy` 
ADD COLUMN `refund_rate` DECIMAL(3,2) NOT NULL AFTER `description`;

ALTER TABLE `eventspace`.`booking_history` 
ADD COLUMN `refund_amout` DECIMAL(14,4) NULL DEFAULT NULL AFTER `reservation_status`;


DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_INSERT` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,null,New.action_taker,now());
END$$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	declare isPaid int;
    declare refund decimal;
    
    set isPaid=0;
    if exists(SELECT booking from booking_history where booking=New.id and reservation_status=3) 
    then
    if New.reservation_status=4
    then
    set isPaid=1;
    end if;
    end if;
    
    if isPaid=1 then
    set refund=(select(round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date)/3600,2)
				*space.rate_per_hour+
				COALESCE(sum(booking_has_extra_amenity.number*booking_has_extra_amenity.rate),0))*refund_rate
				FROM booking 
				join space 
				on booking.space=space.id 
				left join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking
                join cancellation_policy
                on cancellation_policy.id=space.cancellation_policy
				where booking.id=New.id);
    end if;            
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,refund,New.action_taker,now());
END$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
