-- MySQL Workbench Synchronization
-- Generated: 2017-07-07 14:09
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `measurement_unit` INT(11) NOT NULL AFTER `notice_period`,
ADD COLUMN `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `measurement_unit`,
ADD COLUMN `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `created_at`,
ADD INDEX `fk_space_measurement_unit1_idx` (`measurement_unit` ASC);

CREATE TABLE IF NOT EXISTS `eventspace`.`measurement_unit` (
  `id` INT(11) NOT NULL,
  `unit_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unit_type_UNIQUE` (`unit_type` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `eventspace`.`space` 
ADD CONSTRAINT `fk_space_measurement_unit1`
  FOREIGN KEY (`measurement_unit`)
  REFERENCES `eventspace`.`measurement_unit` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


  
INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('5.0');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
