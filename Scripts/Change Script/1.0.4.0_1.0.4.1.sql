-- MySQL Workbench Synchronization
-- Generated: 2017-06-12 15:27
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space_unavailablity` 
ADD COLUMN `is_blocked` TINYINT(1) NOT NULL DEFAULT '1'  AFTER `to_date`;

CREATE TABLE IF NOT EXISTS `eventspace`.`search_address` (
  `space` INT(11) NOT NULL,
  `address` VARCHAR(90) NOT NULL,
  INDEX `fk_search_address_space1_idx` (`space` ASC),
  UNIQUE INDEX `space_UNIQUE` (`space` ASC),
  PRIMARY KEY (`space`),
  CONSTRAINT `fk_search_address_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	declare isPaid int;
	declare refund decimal;
	declare duration int;
	declare cancellationPolicy int;

	set isPaid= 0;
	if exists(SELECT booking from booking_history where booking= New.id and reservation_status= 3) 
		then
	if New.reservation_status = 4
		then
	set isPaid= 1;
	end if;
	end if;
		
	if isPaid= 1 then
	set cancellationPolicy= (select s.cancellation_policy from booking b, space s where s.id = b.space and b.id = New.id);
	set duration= (select TIMESTAMPDIFF(hour, now(), from_date)  from booking  where id= New.id);
	if cancellationPolicy = 3  then if duration < 168 then set refund= 0; end if;end if;
	if cancellationPolicy = 2  then if duration < 120 then set refund= 0; end if;end if;
	if cancellationPolicy = 1  then if duration < 48 then set refund= 0; end if;end if;
	if (refund is null) then
	set refund= (select(round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date) / 3600, 2)
		* space.rate_per_hour +
		COALESCE(sum(booking_has_extra_amenity.number * booking_has_extra_amenity.rate), 0)) * refund_rate
					FROM booking 
	join space 
	on booking.space = space.id
	left join booking_has_extra_amenity 
	on booking.id = booking_has_extra_amenity.booking
	join cancellation_policy
	on cancellation_policy.id = space.cancellation_policy
	where booking.id = New.id);
	end if;   
	end if; 
    
	if new.reservation_status =2 then  
    if OLD.reservation_status!=3 then 
	INSERT INTO `eventspace`.`booking_history` #add booking history if itis normal pay
	VALUES(New.id, New.reservation_status, refund, New.action_taker, now(), 0);
	else  # delete paid history for undo pay
    DELETE FROM `eventspace`.`booking_history` WHERE  booking=new.id and reservation_status=3;
	UPDATE `eventspace`.`space_unavailablity` SET is_blocked=0 WHERE(space = new.space and from_date= new.from_date and to_date= new.to_date);
    end if;
    else # add other events
    INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id, New.reservation_status, refund, New.action_taker, now(), 0);
	end if;

	if New.reservation_status = 3 # add into space unavailablity if booking is paid
		 then
	INSERT INTO `eventspace`.`space_unavailablity`
	VALUES(0, new.space, new.from_date, new.to_date,1);
	end if;
	if isPaid= 1 then #is cancel after paid
	 UPDATE `eventspace`.`space_unavailablity`
     SET is_blocked=0
	WHERE(space = new.space and from_date= new.from_date and to_date= new.to_date);
	end if;
		  
	if New.reservation_status = 2 then #add expire date after approve a booking
    INSERT INTO `eventspace`.`booking_expire_details` (`booking`,`expire_date`) 
    VALUES (new.id,DATE_ADD(now(), INTERVAL 1 DAY));
	end if;
END$$


DELIMITER ;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.1');
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
