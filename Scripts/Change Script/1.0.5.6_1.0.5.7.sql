-- MySQL Workbench Synchronization
-- Generated: 2017-07-27 15:26
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`block_availability` 
ADD COLUMN `menu_items` VARCHAR(45) NULL DEFAULT NULL AFTER `to_time`;

ALTER TABLE `eventspace`.`user_additional_details` 
ADD COLUMN `commission_percentage` INT(11) NULL DEFAULT NULL AFTER `job`;


INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('5.7');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
