-- MySQL Workbench Synchronization
-- Generated: 2017-02-23 13:55
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`bookingCharge`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `bookingCharge`(in bookingId int)
BEGIN
	SELECT (truncate(time_to_sec(TIMEDIFF(booking.to_date,booking.from_date))/3600
				*space.rate_per_hour,2)) 
				+sum(booking_has_extra_amenity.number 
				*booking_has_extra_amenity.rate) as hiring_cost 
				FROM booking 
				join space 
				on booking.space=space.id 
				join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking 
				where booking.id=bookingId;
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
