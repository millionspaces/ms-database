-- MySQL Workbench Synchronization
-- Generated: 2018-04-23 12:49
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `amenity` 
ADD COLUMN `active` INT(11) NULL DEFAULT 1 AFTER `date_updated`,
ADD COLUMN `priority` INT(11) NULL DEFAULT 1 AFTER `active`;

ALTER TABLE `event_type` 
ADD COLUMN `active` INT(11) NULL DEFAULT 1 AFTER `date_updated`,
ADD COLUMN `priority` INT(11) NULL DEFAULT 1 AFTER `active`;

ALTER TABLE `seating_arrangement` 
ADD COLUMN `active` INT(11) NULL DEFAULT 1 AFTER `date_updated`,
ADD COLUMN `priority` INT(11) NULL DEFAULT 1 AFTER `active`;



INSERT INTO `db_changes` (`version`) VALUES ('9.4');  

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
