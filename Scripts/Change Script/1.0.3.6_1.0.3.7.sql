-- MySQL Workbench Synchronization
-- Generated: 2017-05-22 15:56
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`space_unavailablity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `space` INT(11) NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_space_unavailablity_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_unavailablity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB

ALTER TABLE `eventspace`.`db_changes` 
CHANGE COLUMN `created_at` `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
ADD UNIQUE INDEX `version_UNIQUE` (`version` ASC)

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('3.7');

DELIMITER $$

USE `eventspace`$$
DROP TRIGGER IF EXISTS `eventspace`.`booking_AFTER_UPDATE` $$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	declare isPaid int;
    declare refund decimal;
	declare duration int;
    declare cancellationPolicy int; 
    
    set isPaid=0;
    if exists(SELECT booking from booking_history where booking=New.id and reservation_status=3) 
    then
    if New.reservation_status=4
    then
    set isPaid=1;
    end if;
    end if;
    
    if isPaid=1 then
	set cancellationPolicy=(select s.cancellation_policy from booking b,space s where s.id=b.space and b.id=New.id); 
    set duration=(select TIMESTAMPDIFF(hour,now(),from_date)  from booking  where id=New.id); 
		if cancellationPolicy =3  then if duration < 168 then set refund=0; end if;end if;
		if cancellationPolicy =2  then if duration < 120 then set refund=0; end if;end if;
        if cancellationPolicy =1  then if duration < 48 then set refund=0; end if;end if;
		if (refund is null) then  
			set refund=(select(round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date)/3600,2)
				*space.rate_per_hour+
				COALESCE(sum(booking_has_extra_amenity.number*booking_has_extra_amenity.rate),0))*refund_rate
				FROM booking 
				join space 
				on booking.space=space.id 
				left join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking
                join cancellation_policy
                on cancellation_policy.id=space.cancellation_policy
				where booking.id=New.id);
			end if;   
     end if;  
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,refund,New.action_taker,now(),0);
    
    if New.reservation_status=3
     then
     INSERT INTO `eventspace`.`space_unavailablity`
	 VALUES(0,new.space,new.from_date,new.to_date);
     end if;
      if isPaid=1 then
      delete FROM `eventspace`.`space_unavailablity`
      WHERE (space=new.space and from_date=new.from_date and to_date=new.to_date);
      end if;
END$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;