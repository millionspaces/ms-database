-- MySQL Workbench Synchronization
-- Generated: 2017-04-17 16:36
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`filterSpace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `filterSpace`(in params1 int,in params2 int,in rate1 double,in rate2 double,in amenityList TEXT,in eventList TEXT, in limitFor int)
BEGIN
		select dataset2.*,dataset1.count_event, dataset1.count_amenity from
			(
			(select x.space as id, COALESCE(x.count_event, 0) as count_event, COALESCE(y.count_amenity, 0) as count_amenity  from
				(select space, count(event_type) as count_event from space_has_event_type where   FIND_IN_SET (event_type, eventList) group by space) as x
						left join
				(select space, count(amenity) as count_amenity from space_has_amenity where  FIND_IN_SET (amenity, amenityList) group by space) as y
						on x.space = y.space
			)
		union
			(select y.space as id, COALESCE(x.count_event, 0) as count_event, COALESCE(y.count_amenity, 0) as count_amenity from
				(select space, count(event_type) as count_event from space_has_event_type where  FIND_IN_SET (event_type, eventList) group by space) as x
						right join
				(select space, count(amenity) as count_amenity from space_has_amenity where  FIND_IN_SET (amenity, amenityList) group by space) as y
						on x.space = y.space
			)
				  ) as dataset1
		right join
			(select * from space WHERE participant_count>= params1 and participant_count<= params2 and rate_per_hour>= rate1 and rate_per_hour<= rate2 and  approved= 1) as dataset2
		on  dataset1.id = dataset2.id 	group by dataset2.id
		order by  dataset1.count_event desc, dataset1.count_amenity desc limit limitFor;
END$$

DELIMITER ;

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`spaceByCoordinates`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spaceByCoordinates`(in latitudeVal varchar(50),in longitudeVal varchar(50),in radius int)
BEGIN
	SELECT * 
    from space 
    where approved=1 and ( 6371 * acos( cos( radians(latitudeVal) ) * 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - radians(longitudeVal) ) + 
    sin( radians(latitudeVal) ) * 
    sin( radians( latitude ) ) ) )  < radius;

END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
