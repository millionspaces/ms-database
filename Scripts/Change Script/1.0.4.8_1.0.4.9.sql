-- MySQL Workbench Synchronization
-- Generated: 2017-07-05 11:13
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`user` 
CHANGE COLUMN `image_url` `image_url` VARCHAR(450) NULL DEFAULT NULL AFTER `password`,
ADD COLUMN `active` SMALLINT(1) NOT NULL DEFAULT 0 AFTER `is_trusted_owner`,
ADD COLUMN `user_role` VARCHAR(5) NULL DEFAULT 'USER' AFTER `active`;

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `notice_period` INT(11) NULL DEFAULT NULL AFTER `buffer_time`;

CREATE TABLE IF NOT EXISTS `eventspace`.`user_additional_details` (
  `user` INT(11) NOT NULL,
  `last_name` VARCHAR(45) NULL DEFAULT NULL,
  `address` VARCHAR(90) NULL DEFAULT NULL,
  `job` VARCHAR(45) NULL DEFAULT NULL,
  `company_name` VARCHAR(45) NULL DEFAULT NULL,
  `about` VARCHAR(450) NULL DEFAULT NULL,
  INDEX `fk_user_additional_details_user1_idx` (`user` ASC),
  CONSTRAINT `fk_user_additional_details_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('4.9');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
