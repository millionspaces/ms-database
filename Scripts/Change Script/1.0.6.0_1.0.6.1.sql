-- MySQL Workbench Synchronization
-- Generated: 2017-08-24 16:04
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `eventspace`.`featured_spaces` (
 `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT(11) NOT NULL,
  `featured_type` INT(11) NOT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  `featured` TINYINT(1) NOT NULL,
  INDEX `fk_featured_spaces_space1_idx` (`space` ASC),
  PRIMARY KEY (`space`),
  INDEX `fk_featured_spaces_featured_type1_idx` (`featured_type` ASC),
  CONSTRAINT `fk_featured_spaces_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_featured_spaces_featured_type1`
    FOREIGN KEY (`featured_type`)
    REFERENCES `eventspace`.`featured_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `eventspace`.`featured_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `rate` DOUBLE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('6.1');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
