-- MySQL Workbench Synchronization
-- Generated: 2017-12-15 10:05
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `mspaces_live`.`space` 
ADD COLUMN `approved_date` TIMESTAMP NULL DEFAULT now() AFTER `link_space`;

ALTER TABLE `mspaces_live`.`space_unavailablity` 
ADD COLUMN `booking_id` INT(11) NULL DEFAULT NULL AFTER `reservation_status`;


DELIMITER $$
DROP TRIGGER IF EXISTS mspaces_live.space_BEFORE_UPDATE$$
USE `mspaces_live`$$
CREATE  TRIGGER `mspaces_live`.`space_BEFORE_UPDATE` BEFORE UPDATE ON `space` FOR EACH ROW
BEGIN
    if NEW.approved=1 then
		if OLD.approved_date IS NULL then
		SET NEW.approved_date = now();
        end if;
    end if;
END
DELIMITER ;



INSERT INTO `mspaces_live`.`db_changes` (`version`) VALUES ('7.7');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
