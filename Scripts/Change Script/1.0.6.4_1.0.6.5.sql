-- MySQL Workbench Synchronization
-- Generated: 2017-09-11 16:54
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Rasela

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `eventspace`.`space` 
ADD COLUMN `parent_id` INT(11) NULL DEFAULT NULL AFTER `updated_at`,
ADD INDEX `fk_space_space1_idx` (`parent_id` ASC);

ALTER TABLE `eventspace`.`space` 
ADD CONSTRAINT `fk_space_space1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `eventspace`.`space` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  INSERT INTO `eventspace`.`db_changes` (`version`) VALUES ('6.5');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
