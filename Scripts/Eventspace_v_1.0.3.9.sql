-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema eventspace
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema eventspace
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `eventspace` DEFAULT CHARACTER SET utf8 ;
USE `eventspace` ;

-- -----------------------------------------------------
-- Table `eventspace`.`cancellation_policy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`cancellation_policy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(450) NULL,
  `refund_rate` DECIMAL(3,2) NOT NULL, 
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(90) NOT NULL,
  `phone` VARCHAR(45) NULL,
  `password` VARCHAR(450) NOT NULL,
  `is_trusted_owner` SMALLINT(1) NOT NULL DEFAULT 0,
  `image_url` VARCHAR(450) NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity_unit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `amenity_unit` INT NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_amenities_amenities_unit_idx` (`amenity_unit` ASC),
  CONSTRAINT `fk_amenities_amenity_unit`
    FOREIGN KEY (`amenity_unit`)
    REFERENCES `eventspace`.`amenity_unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`event_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address_line_1` VARCHAR(90) NULL,
  `address_line_2` VARCHAR(90) NULL,
  `description` VARCHAR(450) NULL,
  `size` INT NOT NULL,
  `participant_count` INT NOT NULL,
  `rate_per_hour` DECIMAL(8,2) NOT NULL,
  `user` INT NOT NULL,
  `cancellation_policy` INT NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `thumbnail_image` VARCHAR(45) NOT NULL,
  `approved` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_space_user1_idx` (`user` ASC),
  INDEX `fk_space_cancellation_policies1_idx` (`cancellation_policy` ASC),
  CONSTRAINT `fk_space_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_cancellation_policies1`
    FOREIGN KEY (`cancellation_policy`)
    REFERENCES `eventspace`.`cancellation_policy` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reservation_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reservation_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user` INT NOT NULL,
  `space` INT NOT NULL,
  `reservation_status` INT NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  `action_taker` INT NOT NULL,
  `event_type` INT NOT NULL,
  INDEX `fk_user_has_space_space1_idx` (`space` ASC),
  INDEX `fk_user_has_space_user1_idx` (`user` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_booking_reservation_status1_idx` (`reservation_status` ASC),
  INDEX `fk_booking_event_type1_idx` (`event_type` ASC),
  CONSTRAINT `fk_user_has_space_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_space_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_event_type1`
    FOREIGN KEY (`event_type`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reviews` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rate` VARCHAR(10) NOT NULL,
  `title` VARCHAR(50) NULL,
  `description` VARCHAR(255) NULL,
  `created_at` DATETIME NOT NULL,
  `booking_id` INT NOT NULL,
  PRIMARY KEY (`id`, `booking_id`),
  INDEX `fk_reviews_booking1_idx` (`booking_id` ASC),
  UNIQUE INDEX `booking_id_UNIQUE` (`booking_id` ASC),
  CONSTRAINT `fk_reviews_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_amenity` (
  `space` INT NOT NULL,
  `amenity` INT NOT NULL,
  PRIMARY KEY (`space`, `amenity`),
  INDEX `fk_space_has_amenities_amenities1_idx` (`amenity` ASC),
  INDEX `fk_space_has_amenities_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_amenity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_amenity_amenities1`
    FOREIGN KEY (`amenity`)
    REFERENCES `eventspace`.`amenity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_extra_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_extra_amenity` (
  `space` INT NOT NULL,
  `amenity` INT NOT NULL,
  `extra_rate` DECIMAL(8,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`space`, `amenity`),
  INDEX `fk_space_has_amenities1_amenities1_idx` (`amenity` ASC),
  INDEX `fk_space_has_amenities1_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_amenities1_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_amenities1_amenities1`
    FOREIGN KEY (`amenity`)
    REFERENCES `eventspace`.`amenity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NOT NULL,
  `url` VARCHAR(450) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_image_space1_idx` (`space` ASC),
  CONSTRAINT `fk_image_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_unavailablity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_unavailablity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `space` INT(11) NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_space_unavailablity_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_unavailablity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB



-- -----------------------------------------------------
-- Table `eventspace`.`space_has_event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_event_type` (
  `space` INT NOT NULL,
  `event_type` INT NOT NULL,
  PRIMARY KEY (`space`, `event_type`),
  INDEX `fk_space_has_event_type_event_type1_idx` (`event_type` ASC),
  INDEX `fk_space_has_event_type_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_event_type_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_event_type_event_type1`
    FOREIGN KEY (`event_type`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_has_extra_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_has_extra_amenity` (
  `booking` INT NOT NULL,
  `space_has_extra_amenity_space` INT NOT NULL,
  `space_has_extra_amenity_amenity` INT NOT NULL,
  `number` INT NOT NULL,
  `rate` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`booking`, `space_has_extra_amenity_space`, `space_has_extra_amenity_amenity`),
  INDEX `fk_booking_has_space_has_extra_amenity_space_has_extra_amen_idx` (`space_has_extra_amenity_space` ASC, `space_has_extra_amenity_amenity` ASC),
  INDEX `fk_booking_has_space_has_extra_amenity_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_booking_has_space_has_extra_amenity_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_has_space_has_extra_amenity_space_has_extra_amenity1`
    FOREIGN KEY (`space_has_extra_amenity_space` , `space_has_extra_amenity_amenity`)
    REFERENCES `eventspace`.`space_has_extra_amenity` (`space` , `amenity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_history` (
  `booking` INT NOT NULL,
  `reservation_status` INT NOT NULL,
  `refund_amount` DECIMAL(14,4) NULL,
  `action_taker` INT NOT NULL,
  `created_at` DATETIME NULL,
  `seen` TINYINT(1) NOT NULL DEFAULT 0,
  INDEX `fk_table1_booking1_idx` (`booking` ASC),
  INDEX `fk_table1_reservation_status1_idx` (`reservation_status` ASC),
  PRIMARY KEY (`booking`, `reservation_status`),
  INDEX `fk_booking_history_user1_idx` (`action_taker` ASC),
  WITH NO CHECK 
  CONSTRAINT `fk_table1_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_history_user1`
    FOREIGN KEY (`action_taker`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `eventspace`.`payment_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`payment_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `booking` INT(11) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_history_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_payment_history_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB


-- -----------------------------------------------------
-- Table `eventspace`.`booking_expire_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_expire_details` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `booking` INT(11) NOT NULL,
  `expire_date` DATETIME NOT NULL,
  `expired` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_booking_expire_details_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_booking_expire_details_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB

-- -----------------------------------------------------
-- Table `eventspace`.`db_changes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`db_changes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `version` VARCHAR(45) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`)),
  UNIQUE INDEX `version_UNIQUE` (`version` ASC)

ENGINE = InnoDB





USE `eventspace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,null,New.action_taker,now(),0);
END$$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	declare isPaid int;
	declare refund decimal;
	declare duration int;
	declare cancellationPolicy int;

	set isPaid= 0;
	if exists(SELECT booking from booking_history where booking= New.id and reservation_status= 3) 
		then
	if New.reservation_status = 4
		then
	set isPaid= 1;
	end if;
	end if;
		
	if isPaid= 1 then
	set cancellationPolicy= (select s.cancellation_policy from booking b, space s where s.id = b.space and b.id = New.id);
	set duration= (select TIMESTAMPDIFF(hour, now(), from_date)  from booking  where id= New.id);
	if cancellationPolicy = 3  then if duration < 168 then set refund= 0; end if;end if;
	if cancellationPolicy = 2  then if duration < 120 then set refund= 0; end if;end if;
	if cancellationPolicy = 1  then if duration < 48 then set refund= 0; end if;end if;
	if (refund is null) then
	set refund= (select(round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date) / 3600, 2)
		* space.rate_per_hour +
		COALESCE(sum(booking_has_extra_amenity.number * booking_has_extra_amenity.rate), 0)) * refund_rate
					FROM booking 
	join space 
	on booking.space = space.id
	left join booking_has_extra_amenity 
	on booking.id = booking_has_extra_amenity.booking
	join cancellation_policy
	on cancellation_policy.id = space.cancellation_policy
	where booking.id = New.id);
	end if;   
	end if;  
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id, New.reservation_status, refund, New.action_taker, now(), 0);

	if New.reservation_status = 3
		 then
	INSERT INTO `eventspace`.`space_unavailablity`
	VALUES(0, new.space, new.from_date, new.to_date);
	end if;
	if isPaid= 1 then #is cancel after paid
	delete FROM`eventspace`.`space_unavailablity`
	WHERE(space = new.space and from_date= new.from_date and to_date= new.to_date);
	end if;
		  
	if New.reservation_status = 2 then
    INSERT INTO `eventspace`.`booking_expire_details` (`booking`,`expire_date`) 
    VALUES (new.id,DATE_ADD(now(), INTERVAL 1 DAY));
	end if;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`deleteSpace`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`deleteSpace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteSpace`(in  p_spaceId int(11))
begin
declare returnVal Boolean default 0;
IF NOT EXISTS (select * from booking where space=p_spaceId) THEN
	BEGIN
		delete image,space_has_extra_amenity,space_has_amenity,space_has_event_type
		from space
		join image
			on space.id=image.space
		join space_has_amenity
			on space.id=space_has_amenity.space
		join space_has_event_type
			on space.id=space_has_event_type.space
		join space_has_extra_amenity
			on space.id=space_has_extra_amenity.space
		where space.id=p_spaceId;
		
		delete space from space where space.id=p_spaceId;
        set returnVal = 1;
	END;

end if;
select returnVal;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`filterSpace`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`filterSpace`;


DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `filterSpace`(in params1 int,in params2 int,in rate1 double,in rate2 double,in amenityList TEXT,in eventList TEXT, in limitFor int)
BEGIN
		select dataset2.*,dataset1.count_event, dataset1.count_amenity from
			(
			(select x.space as id, COALESCE(x.count_event, 0) as count_event, COALESCE(y.count_amenity, 0) as count_amenity  from
				(select space, count(event_type) as count_event from space_has_event_type where   FIND_IN_SET (event_type, eventList) group by space) as x
						left join
				(select space, count(amenity) as count_amenity from space_has_amenity where  FIND_IN_SET (amenity, amenityList) group by space) as y
						on x.space = y.space
			)
		union
			(select y.space as id, COALESCE(x.count_event, 0) as count_event, COALESCE(y.count_amenity, 0) as count_amenity from
				(select space, count(event_type) as count_event from space_has_event_type where  FIND_IN_SET (event_type, eventList) group by space) as x
						right join
				(select space, count(amenity) as count_amenity from space_has_amenity where  FIND_IN_SET (amenity, amenityList) group by space) as y
						on x.space = y.space
			)
				  ) as dataset1
		right join
			(select * from space WHERE participant_count>= params1 and participant_count<= params2 and rate_per_hour>= rate1 and rate_per_hour<= rate2 and  approved= 1) as dataset2
		on  dataset1.id = dataset2.id 	group by dataset2.id
		order by  dataset1.count_event desc, dataset1.count_amenity desc limit limitFor;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`averageRating`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`averageRating`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `averageRating`(in spaceId int)
BEGIN
	SELECT round(avg(SUBSTRING_INDEX(r.rate,'|',1)),2) as average 
    FROM booking b, reviews r, space s 
    where b.id=r.booking_id 
    and s.id=b.space 
    and s.id=spaceId;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`spaceByCoordinates`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`spaceByCoordinates`;

DELIMITER $$

USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spaceByCoordinates`(in latitudeVal varchar(50),in longitudeVal varchar(50),in radius int)
BEGIN
	SELECT * 
    from space 
    where approved=1 and ( 6371 * acos( cos( radians(latitudeVal) ) * 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - radians(longitudeVal) ) + 
    sin( radians(latitudeVal) ) * 
    sin( radians( latitude ) ) ) )  < radius;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`bookingCharge`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`bookingCharge`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `bookingCharge`(in bookingId int)
BEGIN
	SELECT (round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date)/3600,2)
				*space.rate_per_hour+
				COALESCE(sum(booking_has_extra_amenity.number*booking_has_extra_amenity.rate),0))*1
				FROM booking 
				join space 
				on booking.space=space.id 
				left join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking
				where booking.id=bookingId;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`isCancelledAfterPaid`
-- -----------------------------------------------------


USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`isCancelledAfterPaid`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `isCancelledAfterPaid`(in booking_id int)
BEGIN
	DECLARE b_id int(11) DEFAULT null;
    DECLARE b_status int(11) DEFAULT null;
    DECLARE is_cancelled_after_paid boolean default false;
    DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE cursor_name CURSOR FOR select booking,reservation_status from booking_history where booking=booking_id;
    
     -- declare NOT FOUND handler
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    IF EXISTS (SELECT booking from booking_history where booking=booking_id and reservation_status=4) THEN
		BEGIN
			OPEN cursor_name;
			get_list: LOOP
				FETCH cursor_name INTO b_id,b_status;
				IF v_finished = 1 THEN 
					LEAVE get_list;
				END IF;
				
				IF
					b_status=3
					THEN
						SET is_cancelled_after_paid=TRUE;
						SELECT is_cancelled_after_paid;
						LEAVE get_list;
				END IF;
			END LOOP get_list;
            SELECT is_cancelled_after_paid;
		END;
	END IF;
    SELECT is_cancelled_after_paid;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`isCancelledAfterPaid`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`getCancelledDate`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCancelledDate`(in bookingId int, in bookingStatus int)
BEGIN
	select created_at from booking_history where booking=bookingId and reservation_status=bookingStatus;
END$$

DELIMITER ;


ALTER TABLE `eventspace`.`reservation_status` 
ADD COLUMN `label` VARCHAR(45) NULL DEFAULT NULL AFTER `name`;

-- -----------------------------------------------------
-- StoredProcedure `eventspace`.`isCancelledAfterPaid`
-- -----------------------------------------------------

USE `eventspace`;
DROP procedure IF EXISTS `eventspace`.`forceDeleteSpace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `forceDeleteSpace`(in  p_spaceId int(11))
begin
declare returnVal Boolean default 0;
IF EXISTS (select * from booking where space=p_spaceId) THEN
	BEGIN
		delete booking_has_extra_amenity
			from booking_has_extra_amenity
			where space_has_extra_amenity_space=p_spaceId;
        
        delete booking_history, reviews
        from booking
			left join booking_history
				on booking.id=booking_history.booking
			left join reviews
				on booking.id=reviews.booking_id
        where booking.space=p_spaceId;
        
        delete booking
			from booking
			where booking.space=p_spaceId;
       
	end;
    end if;
	delete image,space_has_amenity,space_has_event_type,space_has_extra_amenity
	from space
	left join image
		on space.id=image.space
	left join space_has_amenity
		on space.id=space_has_amenity.space
	left join space_has_event_type
		on space.id=space_has_event_type.space
	left join space_has_extra_amenity
		on space.id= space_has_extra_amenity.space
	where space.id=p_spaceId;
	
	delete space from space where space.id=p_spaceId;
	set returnVal = 1;
select returnVal;
end$$

DELIMITER ;