-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema eventspace
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema eventspace
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `eventspace` DEFAULT CHARACTER SET utf8 ;
USE `eventspace` ;

-- -----------------------------------------------------
-- Table `eventspace`.`cancellation_policy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`cancellation_policy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(450) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(90) NOT NULL,
  `phone` VARCHAR(45) NULL,
  `password` VARCHAR(450) NOT NULL,
  `is_trusted_owner` SMALLINT(1) NOT NULL DEFAULT 0,
  `image_url` VARCHAR(450) NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity_unit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `amenity_unit` INT NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_amenities_amenities_unit_idx` (`amenity_unit` ASC),
  CONSTRAINT `fk_amenities_amenity_unit`
    FOREIGN KEY (`amenity_unit`)
    REFERENCES `eventspace`.`amenity_unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`event_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address_line_1` VARCHAR(90) NULL,
  `address_line_2` VARCHAR(90) NULL,
  `description` VARCHAR(450) NULL,
  `size` INT NOT NULL,
  `participant_count` INT NOT NULL,
  `rate_per_hour` DECIMAL(8,2) NOT NULL,
  `user` INT NOT NULL,
  `cancellation_policy` INT NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_space_user1_idx` (`user` ASC),
  INDEX `fk_space_cancellation_policies1_idx` (`cancellation_policy` ASC),
  CONSTRAINT `fk_space_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_cancellation_policies1`
    FOREIGN KEY (`cancellation_policy`)
    REFERENCES `eventspace`.`cancellation_policy` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reservation_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reservation_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user` INT NOT NULL,
  `space` INT NOT NULL,
  `reservation_status` INT NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  `action_taker` INT NOT NULL,
  `event_type` INT NOT NULL,
  INDEX `fk_user_has_space_space1_idx` (`space` ASC),
  INDEX `fk_user_has_space_user1_idx` (`user` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_booking_reservation_status1_idx` (`reservation_status` ASC),
  INDEX `fk_booking_event_type1_idx` (`event_type` ASC),
  CONSTRAINT `fk_user_has_space_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_space_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_event_type1`
    FOREIGN KEY (`event_type`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reviews` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rate` INT NOT NULL,
  `description` VARCHAR(45) NULL,
  `created_at` DATETIME NOT NULL,
  `booking_id` INT NOT NULL,
  PRIMARY KEY (`id`, `booking_id`),
  INDEX `fk_reviews_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_reviews_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_amenity` (
  `space` INT NOT NULL,
  `amenity` INT NOT NULL,
  PRIMARY KEY (`space`, `amenity`),
  INDEX `fk_space_has_amenities_amenities1_idx` (`amenity` ASC),
  INDEX `fk_space_has_amenities_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_amenity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_amenity_amenities1`
    FOREIGN KEY (`amenity`)
    REFERENCES `eventspace`.`amenity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_extra_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_extra_amenity` (
  `space` INT NOT NULL,
  `amenity` INT NOT NULL,
  `extra_rate` DECIMAL(8,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`space`, `amenity`),
  INDEX `fk_space_has_amenities1_amenities1_idx` (`amenity` ASC),
  INDEX `fk_space_has_amenities1_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_amenities1_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_amenities1_amenities1`
    FOREIGN KEY (`amenity`)
    REFERENCES `eventspace`.`amenity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NOT NULL,
  `url` VARCHAR(450) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_image_space1_idx` (`space` ASC),
  CONSTRAINT `fk_image_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_event_type` (
  `space` INT NOT NULL,
  `event_type` INT NOT NULL,
  PRIMARY KEY (`space`, `event_type`),
  INDEX `fk_space_has_event_type_event_type1_idx` (`event_type` ASC),
  INDEX `fk_space_has_event_type_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_event_type_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_event_type_event_type1`
    FOREIGN KEY (`event_type`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_has_extra_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_has_extra_amenity` (
  `booking` INT NOT NULL,
  `space_has_extra_amenity_space` INT NOT NULL,
  `space_has_extra_amenity_amenity` INT NOT NULL,
  `number` INT NOT NULL,
  `rate` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`booking`, `space_has_extra_amenity_space`, `space_has_extra_amenity_amenity`),
  INDEX `fk_booking_has_space_has_extra_amenity_space_has_extra_amen_idx` (`space_has_extra_amenity_space` ASC, `space_has_extra_amenity_amenity` ASC),
  INDEX `fk_booking_has_space_has_extra_amenity_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_booking_has_space_has_extra_amenity_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_has_space_has_extra_amenity_space_has_extra_amenity1`
    FOREIGN KEY (`space_has_extra_amenity_space` , `space_has_extra_amenity_amenity`)
    REFERENCES `eventspace`.`space_has_extra_amenity` (`space` , `amenity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_history` (
  `booking` INT NOT NULL,
  `reservation_status` INT NOT NULL,
  `action_taker` INT NOT NULL,
  `created_at` DATETIME NULL,
  INDEX `fk_table1_booking1_idx` (`booking` ASC),
  INDEX `fk_table1_reservation_status1_idx` (`reservation_status` ASC),
  PRIMARY KEY (`booking`, `reservation_status`),
  INDEX `fk_booking_history_user1_idx` (`action_taker` ASC),
  CONSTRAINT `fk_table1_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_history_user1`
    FOREIGN KEY (`action_taker`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `eventspace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,New.action_taker,now());
END$$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,New.action_taker,now());
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
