DROP TABLE `eventspace`.`auto_publish`;
DROP TABLE `eventspace`.`amenity`;
DROP TABLE `eventspace`.`amenity_unit`;
DROP TABLE `eventspace`.`cancellation_policy`;
DROP TABLE `eventspace`.`event_type`;
DROP TABLE `eventspace`.`reservation_status`;
DROP TABLE `eventspace`.`rule`;
DROP TABLE `eventspace`.`seating_arrangement`;
DROP TABLE `eventspace`.`measurement_unit`;
DROP TABLE `eventspace`.`charge_type`;
DROP TABLE `eventspace`.`day`;




-- -----------------------------------------------------
-- Table `eventspace`.`auto_publish`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`auto_publish` (
  `id` INT NOT NULL,
  `enabled` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `amenity_unit` INT NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_amenities_amenities_unit_idx` (`amenity_unit` ASC),
  CONSTRAINT `fk_amenities_amenity_unit`
    FOREIGN KEY (`amenity_unit`)
    REFERENCES `eventspace`.`amenity_unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity_unit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`cancellation_policy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`cancellation_policy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(450) NULL,
  `refund_rate` DECIMAL(3,2) NOT NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`event_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `eventspace`.`reservation_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reservation_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `label` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`rule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`rule` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(45) NULL,
  `icon` VARCHAR(45) NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`seating_arrangement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`seating_arrangement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `eventspace`.`measurement_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`measurement_unit` (
  `id` INT NOT NULL,
  `unit_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unit_type_UNIQUE` (`unit_type` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`day`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`day` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `eventspace`.`charge_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`charge_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

