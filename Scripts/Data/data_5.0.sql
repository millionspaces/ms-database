/*
-- Query: SELECT * FROM eventspace.event_type
LIMIT 0, 1000

-- Date: 2017-01-25 14:33
auto_publish
amenity_unit
amenity
event_type
reservation_status
rule
seating_arrangement
cancellation_policy
measurement_unit
charge_type
day
reimbursable_options
block_charge_type
featured_type
*/



INSERT INTO `eventspace`.`auto_publish` (`id`, `enabled`) VALUES ('1', '1');


INSERT INTO `eventspace`.`amenity_unit` (`id`,`name`) VALUES (1,'Per hour');
INSERT INTO `eventspace`.`amenity_unit` (`id`,`name`) VALUES (2,'Per person');
INSERT INTO `eventspace`.`amenity_unit` (`id`,`name`) VALUES (3,'Per unit');

INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (1,'Air Conditioning',3,'airconditioning','https://s3-us-west-2.amazonaws.com/mobile-icons/airconditioning.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (2,'Breakfast',2,'breakfast','https://s3-us-west-2.amazonaws.com/mobile-icons/breakfast.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (3,'Coffee',2,'coffee','https://s3-us-west-2.amazonaws.com/mobile-icons/coffee.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (4,'DVD Player',1,'dvdplayer','https://s3-us-west-2.amazonaws.com/mobile-icons/dvdplayer.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (5,'Dinner',2,'dinner','https://s3-us-west-2.amazonaws.com/mobile-icons/dinner.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (6,'Disabled Access',1,'disabledaccess','https://s3-us-west-2.amazonaws.com/mobile-icons/disabledaccess.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (7,'Flip Chart',1,'flipchart','https://s3-us-west-2.amazonaws.com/mobile-icons/flipchart.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (8,'Lunch',2,'lunch','https://s3-us-west-2.amazonaws.com/mobile-icons/lunch.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (9,'Microphone',1,'microphone ','https://s3-us-west-2.amazonaws.com/mobile-icons/microphone.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (10,'Parking',1,'parking','https://s3-us-west-2.amazonaws.com/mobile-icons/parking.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (11,'Pillar-Free',3,'pillarfree','https://s3-us-west-2.amazonaws.com/mobile-icons/pillarfree.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (12,'Projector and Screen',1,'projectorandscreen','https://s3-us-west-2.amazonaws.com/mobile-icons/projectorandscreen.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (13,'Smart TV',1,'smarttv','https://s3-us-west-2.amazonaws.com/mobile-icons/smarttv.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (14,'Snacks',2,'snacks','https://s3-us-west-2.amazonaws.com/mobile-icons/snacks.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (15,'Sound System',1,'soundsystem','https://s3-us-west-2.amazonaws.com/mobile-icons/soundsystem.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (16,'Stage',3,'stage','https://s3-us-west-2.amazonaws.com/mobile-icons/stage.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (17,'Stationery',3,'stationery','https://s3-us-west-2.amazonaws.com/mobile-icons/stationery.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (18,'Valet Parking',3,'valetparking','https://s3-us-west-2.amazonaws.com/mobile-icons/valetparking.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (19,'Water',2,'water','https://s3-us-west-2.amazonaws.com/mobile-icons/water.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (20,'Whiteboard',3,'whiteboard','https://s3-us-west-2.amazonaws.com/mobile-icons/whiteboard.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (21,'WiFi',1,'wifi','https://s3-us-west-2.amazonaws.com/mobile-icons/wifi.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (22,'Chairs', '3', 'chairs', 'https://s3-us-west-2.amazonaws.com/mobile-icons/chairs.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (23,'Tables', '3', 'tables', 'https://s3-us-west-2.amazonaws.com/mobile-icons/tables.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (24,'Bean Bags', '3', 'beanbags', 'https://s3-us-west-2.amazonaws.com/mobile-icons/beanbags.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (25,'Video Conferencing', '1', 'videoconferencing', 'https://s3-us-west-2.amazonaws.com/mobile-icons/videoconferencing.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (26,'Printing', '3', 'printing', 'https://s3-us-west-2.amazonaws.com/mobile-icons/printing.png');
INSERT INTO `eventspace`.`amenity` (`id`,`name`,`amenity_unit`,`icon`,`mobile_icon`) VALUES (27,'Corkage', '3', 'corkage', 'https://s3-us-west-2.amazonaws.com/mobile-icons/corkage.png');


INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (1,'Party','party','https://s3-us-west-2.amazonaws.com/mobile-icons/party.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (2,'Wedding','wedding','https://s3-us-west-2.amazonaws.com/mobile-icons/wedding.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (3,'Press Conference','pressconference','https://s3-us-west-2.amazonaws.com/mobile-icons/pressconference.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (4,'Meeting','meeting','https://s3-us-west-2.amazonaws.com/mobile-icons/meeting.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (5,'Interview','interview','https://s3-us-west-2.amazonaws.com/mobile-icons/interview.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (6,'Training','training','https://s3-us-west-2.amazonaws.com/mobile-icons/training.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (7,'Photoshoot','photoshoot','https://s3-us-west-2.amazonaws.com/mobile-icons/photoshoot.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (8,'Hot Desk','hotdesk','https://s3-us-west-2.amazonaws.com/mobile-icons/hotdesk.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (9,'Sports','sports','https://s3-us-west-2.amazonaws.com/mobile-icons/sports.png');
INSERT INTO `eventspace`.`event_type` (`id`,`name`,`icon`,`mobile_icon`) VALUES (10,'Concert','concert','https://s3-us-west-2.amazonaws.com/mobile-icons/concert.png');

INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('1','INITIATED','Initiated');
INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('2','PENDING_PAYMENT','Pending Payment');
INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('3','PAYMENT_DONE','Confirmed');
INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('4','CANCELLED','Cancelled');
INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('5','CONFIRMED','Confirmed');
INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('6','EXPIRED','Expired');
INSERT INTO `eventspace`.`reservation_status` (`id`,`name`,`label`) VALUES ('7','DISCARDED','Discared');

INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('1', 'No Flame','Flame Allowed', 'noflame');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('2', 'No Smoking','Smoking Allowed', 'nosmoking');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('3', 'No Cooking', 'Cooking Allowed','nocooking');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('4', 'No Ticket Sales','Ticket Sales Allowed', 'noticketsales');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('5', 'No Music','Music Allowed', 'nomusic');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('6', 'No Outside Catering/Food','Outside Catering/Food Allowed', 'nooutsidefood');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('7', 'No Alcohol (Serving)','Alcohol (Serving) Allowed', 'noalcoholserving');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('8', 'No Alcohol (Selling)','Alcohol (Selling) Allowed', 'noalcoholselling');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('9', 'No Loud Music/Dancing','Loud Music/Dancing Allowed', 'noloudmusic');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('10', 'No Pets Allowed','Pets Allowed', 'nopetsallowed');
INSERT INTO `eventspace`.`rule` (`id`, `name`,`display_name`, `icon`) VALUES ('11', 'No Children Under 12','Children Under 12 Allowed', 'nochildren');



INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('1', 'Standing', 'standard-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/standard-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('2', 'Banquet', 'banquet-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/banquet-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('3', 'Cocktail', 'cocktail-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/concert.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('4', 'Classroom', 'classroom-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/cocktail-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('5', 'Theatre', 'thetre-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/theatre-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('6', 'U-Shape', 'ushape-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/concert.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('7', 'Boardroom', 'boardroom-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/ushape-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('8', 'Cabaret', 'caberet-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/cabaret-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('9', 'Hollow-Square', 'hollow-icon.jpg','https://s3-us-west-2.amazonaws.com/mobile-icons/hollow-icon.png');
INSERT INTO `eventspace`.`seating_arrangement` (`id`, `name`, `icon`,`mobile_icon`) VALUES ('10', 'Standard', 'sandard.jpg', 'https://s3-us-west-2.amazonaws.com/mobile-icons/sandard.png');

INSERT INTO `eventspace`.`cancellation_policy` (`name`, `description`, `refund_rate`) VALUES ('Flexible', 'Full refund up to two days prior to event', '1.00');
INSERT INTO `eventspace`.`cancellation_policy` (`name`, `description`, `refund_rate`) VALUES ('Moderate', '50% refund up to one week prior to event', '0.50');
INSERT INTO `eventspace`.`cancellation_policy` (`name`, `description`, `refund_rate`) VALUES ('Strict', 'No refund', '0.00');


INSERT INTO `eventspace`.`measurement_unit` (`id`, `unit_type`) VALUES ('1', 'sq ft');
INSERT INTO `eventspace`.`measurement_unit` (`id`, `unit_type`) VALUES ('2', 'sq m');

INSERT INTO `eventspace`.`charge_type` (`id`, `name`) VALUES ('1', 'HOUR_BASE');
INSERT INTO `eventspace`.`charge_type` (`id`, `name`) VALUES ('2', 'BLOCK_BASE');



INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('1', 'monday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('2', 'tuesday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('3', 'wednesday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('4', 'thursday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('5', 'friday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('6', 'saturday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('7', 'sunday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('8', 'weekdays');


INSERT INTO `eventspace`.`reimbursable_options` (`id`, `code`, `desc`) VALUES ('1', 'Space only charge', 'desc');
INSERT INTO `eventspace`.`reimbursable_options` (`id`, `code`, `desc`) VALUES ('2', 'Reimbursable against guest bill', 'desc');

INSERT INTO `eventspace`.`block_charge_type` (`id`, `name`) VALUES ('1', 'space only charge');
INSERT INTO `eventspace`.`block_charge_type` (`id`, `name`) VALUES ('2', 'Per guest based charge');


INSERT INTO `eventspace`.`featured_type` (`id`, `name`, `rate`) VALUES ('1', 'type1 ','100');

INSERT INTO `device` (`id`, `name`) VALUES ('1', 'Web');
INSERT INTO `device` (`id`, `name`) VALUES ('2', 'IOS');
INSERT INTO `device` (`id`, `name`) VALUES ('3', 'Android');




