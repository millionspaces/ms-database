/*
-- Query: SELECT * FROM eventspace.reservation_status
LIMIT 0, 1000

-- Date: 2017-03-09 13:33
*/
UPDATE `eventspace`.`reservation_status` SET `label`='Pending Approval' WHERE `id`='1';
UPDATE `eventspace`.`reservation_status` SET `label`='Pending Payment' WHERE `id`='2';
UPDATE `eventspace`.`reservation_status` SET `label`='Payment Done' WHERE `id`='3';
UPDATE `eventspace`.`reservation_status` SET `label`='Cancelled' WHERE `id`='4';
UPDATE `eventspace`.`reservation_status` SET `label`='Confirmed' WHERE `id`='5';

