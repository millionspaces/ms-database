UPDATE `eventspace`.`rule` SET `display_name`='Flame Allowed' WHERE `id`='1';
UPDATE `eventspace`.`rule` SET `display_name`='Smoking Allowed' WHERE `id`='2';
UPDATE `eventspace`.`rule` SET `display_name`='Cooking Allowed' WHERE `id`='3';
UPDATE `eventspace`.`rule` SET `display_name`='Ticket Sales Allowed' WHERE `id`='4';
UPDATE `eventspace`.`rule` SET `display_name`='Music Allowed' WHERE `id`='5';
UPDATE `eventspace`.`rule` SET `display_name`='Outside Catering/Food Allowed' WHERE `id`='6';
UPDATE `eventspace`.`rule` SET `display_name`='Loud Music/Dancing Allowed' WHERE `id`='9';
UPDATE `eventspace`.`rule` SET `display_name`='Alcohol (Serving) Allowed' WHERE `id`='7';
UPDATE `eventspace`.`rule` SET `display_name`='Alcohol (Selling) Allowed' WHERE `id`='8';
UPDATE `eventspace`.`rule` SET `display_name`='Pets Allowed Allowed' WHERE `id`='10';
UPDATE `eventspace`.`rule` SET `display_name`='Children Under 12 Allowed' WHERE `id`='11';
