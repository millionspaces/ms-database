
INSERT INTO `amenity_unit` (`id`,`name`) VALUES (1,'Per hour');
INSERT INTO `amenity_unit` (`id`,`name`) VALUES (2,'Per head');

INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (1,'Party','party');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (2,'Wedding','wedding');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (3,'Press Release','tv');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (4,'Meeting','meeting');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (5,'Interview','interview');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (6,'Training','training');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (7,'Photoshoot','camera');

INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (1,'Micro Phone',1,'microphone');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (2,'Screen',1,'screen');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (3,'Projector',1,'projector');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (4,'Lunch',2,'lunch');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (5,'Dinner',2,'dinner');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (6,'Coffee',2,'coffee');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (7,'Snacks',2,'snacks');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (8,'Water',2,'water');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (9,'Stationaries',2,'stationary');

INSERT INTO `reservation_status` (`name`) VALUES ('PENDING_APPROVAL');
INSERT INTO `reservation_status` (`name`) VALUES ('PENDING_PAYMENT');
INSERT INTO `reservation_status` (`name`) VALUES ('PAYMENT_DONE');
INSERT INTO `reservation_status` (`name`) VALUES ('CANCELLED');
INSERT INTO `reservation_status` (`name`) VALUES ('CONFIRMED');

INSERT INTO `cancellation_policy` (`id`,`name`,`description`) VALUES (1,'Flexible','Full refund upto two days prior to event, except processing fees');
INSERT INTO `cancellation_policy` (`id`,`name`,`description`) VALUES (2,'Moderate','Full refund upto five days prior to event, except processing fees');
INSERT INTO `cancellation_policy` (`id`,`name`,`description`) VALUES (3,'Strict','50% refund up to one week prior to arrival, except processing fees');