INSERT INTO `eventspace`.`measurement_unit` (`id`, `unit_type`) VALUES ('1', 'sq ft');
INSERT INTO `eventspace`.`measurement_unit` (`id`, `unit_type`) VALUES ('2', 'sq m');

update eventspace.space
set measurement_unit=1 where measurement_unit=0;