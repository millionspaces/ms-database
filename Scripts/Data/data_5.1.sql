UPDATE `eventspace`.`amenity` SET `name`='Pillar-Free', `icon`='pillorfree' WHERE `id`='11';

UPDATE `eventspace`.`cancellation_policy` SET `description`='Full refund upto two days prior to event' WHERE `id`='1';
UPDATE `eventspace`.`cancellation_policy` SET `description`='Full refund upto five days prior to event' WHERE `id`='2';
UPDATE `eventspace`.`cancellation_policy` SET `description`='50% refund up to one week prior to arrival' WHERE `id`='3';

UPDATE `eventspace`.`rule` SET `name`='No Alcohol (Serving)', `icon`='noalcoholserving' WHERE `id`='7';
UPDATE `eventspace`.`rule` SET `name`='No Alcohol (Selling)', `icon`='noalcoholselling' WHERE `id`='8';
UPDATE `eventspace`.`rule` SET `name`='No Loud Music/Dancing', `icon`='noloudmusic' WHERE `id`='9';
