INSERT INTO `eventspace`.`charge_type` (`id`, `name`) VALUES ('1', 'HOUR_BASE');
INSERT INTO `eventspace`.`charge_type` (`id`, `name`) VALUES ('2', 'BLOCK_BASE');



INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('1', 'monday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('2', 'tuesday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('3', 'wednesday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('4', 'thursday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('5', 'friday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('6', 'saturday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('7', 'sunday');
INSERT INTO `eventspace`.`day` (`id`, `name`) VALUES ('8', 'weekdays');


update eventspace.space
set measurement_unit=1 where measurement_unit=0;

update eventspace.space
set charge_type=1 where charge_type=0;

update eventspace.block_availability
set day=1 where day=0;



