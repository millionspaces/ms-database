UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/airconditioning.png' WHERE `id`='1';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/breakfast.png' WHERE `id`='2';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/coffee.png' WHERE `id`='3';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/dvdplayer.png' WHERE `id`='4';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/dinner.png' WHERE `id`='5';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/disabledaccess.png' WHERE `id`='6';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/flipchart.png' WHERE `id`='7';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/lunch.png' WHERE `id`='8';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/microphone.png' WHERE `id`='9';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/parking.png' WHERE `id`='10';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/pillarfree.png' WHERE `id`='11';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/projectorandscreen.png' WHERE `id`='12';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/smarttv.png' WHERE `id`='13';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/snacks.png' WHERE `id`='14';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/soundsystem.png' WHERE `id`='15';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/stage.png' WHERE `id`='16';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/stationery.png' WHERE `id`='17';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/valetparking.png' WHERE `id`='18';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/water.png' WHERE `id`='19';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/whiteboard.png' WHERE `id`='20';
UPDATE `eventspace`.`amenity` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/wifi.png' WHERE `id`='21';



UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/party.png' WHERE `id`='1';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/wedding.png' WHERE `id`='2';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/pressconference.png' WHERE `id`='3';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/meeting.png' WHERE `id`='4';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/interview.png' WHERE `id`='5';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/training.png' WHERE `id`='6';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/photoshoot.png' WHERE `id`='7';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/hotdesk.png' WHERE `id`='8';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/sports.png' WHERE `id`='9';
UPDATE `eventspace`.`event_type` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/concert.png' WHERE `id`='10';



UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/standard-icon.jpg' WHERE `id`='1';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/banquet-icon.jpg' WHERE `id`='2';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/cocktail-icon.jpg' WHERE `id`='3';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/classroom-icon.jpg' WHERE `id`='4';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/thetre-icon.jpg' WHERE `id`='5';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/ushape-icon.jpg' WHERE `id`='6';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/boardroom-icon.jpg' WHERE `id`='7';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/caberet-icon.jpg' WHERE `id`='8';
UPDATE `eventspace`.`seating_arrangement` SET `mobile_icon`='https://s3-us-west-2.amazonaws.com/mobile-icons/hollow-icon.jpg' WHERE `id`='9';
