UPDATE `eventspace`.`cancellation_policy` SET `description`='50% refund up to one week prior to event', `refund_rate`='0.50' WHERE `id`='2';
UPDATE `eventspace`.`cancellation_policy` SET `description`='Full refund up to two days prior to event' WHERE `id`='1';
UPDATE `eventspace`.`cancellation_policy` SET `description`='No refund', `refund_rate`='0.00' WHERE `id`='3';
