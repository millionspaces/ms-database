/*
-- Query: SELECT * FROM eventspace.event_type
LIMIT 0, 1000

-- Date: 2017-01-25 14:33
*/
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (1,'Party','party');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (2,'Wedding','wedding');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (3,'Press Release','tv');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (4,'Meeting','meeting');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (5,'Interview','interview');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (6,'Training','training');
INSERT INTO `event_type` (`id`,`name`,`icon`) VALUES (7,'Photoshoot','camera');

INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (1,'Micro Phone',1,'microphone');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (2,'Screen',1,'screen');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (3,'Projector',1,'projector');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (4,'Lunch',2,'lunch');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (5,'Dinner',2,'dinner');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (6,'Coffee',2,'coffee');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (7,'Snacks',2,'snacks');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (8,'Water',2,'water');
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`,`icon`) VALUES (9,'Stationaries',2,'stationary ');
