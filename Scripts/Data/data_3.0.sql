/*
-- Date: 2017-01-23 15:23
*/

INSERT INTO `amenity_unit` (`id`,`name`) VALUES (1,'Per hour');
INSERT INTO `amenity_unit` (`id`,`name`) VALUES (2,'Per head');

INSERT INTO `event_type` (`id`,`name`) VALUES (5,'Interview');
INSERT INTO `event_type` (`id`,`name`) VALUES (4,'Meeting');
INSERT INTO `event_type` (`id`,`name`) VALUES (1,'Party');
INSERT INTO `event_type` (`id`,`name`) VALUES (7,'Photoshoot');
INSERT INTO `event_type` (`id`,`name`) VALUES (3,'Press Release');
INSERT INTO `event_type` (`id`,`name`) VALUES (6,'Training');
INSERT INTO `event_type` (`id`,`name`) VALUES (2,'Wedding');

INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (1,'Micro Phone',1);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (2,'Screen',1);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (3,'Projector',1);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (4,'Lunch',2);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (5,'Dinner',2);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (6,'Coffee',2);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (7,'Snacks',2);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (8,'Water',2);
INSERT INTO `amenity` (`id`,`name`,`amenity_unit`) VALUES (9,'Stationaries',2);



