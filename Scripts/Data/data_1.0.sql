INSERT INTO `eventspace`.`cancellation_policy` (`name`) VALUES ('CANCELLATION-POLICY-1');
INSERT INTO `eventspace`.`cancellation_policy` (`name`) VALUES ('CANCELLATION-POLICY-2');

INSERT INTO `eventspace`.`event_type` (`name`) VALUES ('Corporate');
INSERT INTO `eventspace`.`event_type` (`name`) VALUES ('Parties');
INSERT INTO `eventspace`.`event_type` (`name`) VALUES ('Wedding');

INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Conferences', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Seminars', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Meetings', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Business Dinners', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Trade Shows', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Team Building Events', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Press Conferences', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Networking Events', '1');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Product Launches', '1');

INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Birthday party', '2');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Christmas party', '2');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('New Year Eve party', '2');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Graduation Ball', '2');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Cocktail party', '2');
INSERT INTO `eventspace`.`event_sub_type` (`name`, `event_type`) VALUES ('Fashion show', '2');



