-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema eventspace
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema eventspace
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `eventspace` DEFAULT CHARACTER SET utf8 ;
USE `eventspace` ;

-- -----------------------------------------------------
-- Table `eventspace`.`cancellation_policy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`cancellation_policy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(450) NULL,
  `refund_rate` DECIMAL(3,2) NOT NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(90) NOT NULL,
  `phone` VARCHAR(45) NULL,
  `company_phone` VARCHAR(45) NULL,
  `password` VARCHAR(450) NOT NULL,
  `image_url` VARCHAR(450) NULL,
  `is_trusted_owner` SMALLINT(1) NOT NULL DEFAULT 0,
  `active` SMALLINT(1) NOT NULL DEFAULT 0,
  `user_role` VARCHAR(5) NULL DEFAULT 'USER',
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity_unit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`amenity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `amenity_unit` INT NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  `mobile_icon` VARCHAR(250) NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_amenities_amenities_unit_idx` (`amenity_unit` ASC),
  CONSTRAINT `fk_amenities_amenity_unit`
    FOREIGN KEY (`amenity_unit`)
    REFERENCES `eventspace`.`amenity_unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`event_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NOT NULL,
  `mobile_icon` VARCHAR(250) NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`measurement_unit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`measurement_unit` (
  `id` INT NOT NULL,
  `unit_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unit_type_UNIQUE` (`unit_type` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`charge_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`charge_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address_line_1` VARCHAR(90) NULL,
  `address_line_2` VARCHAR(90) NULL,
  `description` VARCHAR(450) NULL,
  `size` INT NOT NULL,
  `participant_count` INT NOT NULL,
  `rate_per_hour` DECIMAL(8,2) NOT NULL,
  `user` INT NOT NULL,
  `cancellation_policy` INT NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `thumbnail_image` VARCHAR(45) NOT NULL,
  `security_deposit` INT NULL,
  `approved` TINYINT(1) NOT NULL,
  `calendar_start` DATE NULL,
  `calendar_end` DATE NULL,
  `calendar_event_size` VARCHAR(45) NULL,
  `buffer_time` INT NULL,
  `notice_period` INT NULL,
  `measurement_unit` INT NOT NULL,
  `charge_type` INT NOT NULL,
  `min_participant_count` INT NULL,
  `block_charge_type` VARCHAR(45) NULL,
  `menu_file_name` VARCHAR(450) NULL,
  `reimbursable_options` INT NULL,
  `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `parent_id` INT NULL,
  `link_space` INT NULL,
  `approved_date` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_space_user1_idx` (`user` ASC),
  INDEX `fk_space_cancellation_policies1_idx` (`cancellation_policy` ASC),
  INDEX `fk_space_measurement_unit1_idx` (`measurement_unit` ASC),
  INDEX `fk_space_charge_type1_idx` (`charge_type` ASC),
  INDEX `fk_space_space1_idx` (`parent_id` ASC),
  INDEX `fk_space_space2_idx` (`link_space` ASC),
  CONSTRAINT `fk_space_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_cancellation_policies1`
    FOREIGN KEY (`cancellation_policy`)
    REFERENCES `eventspace`.`cancellation_policy` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_measurement_unit1`
    FOREIGN KEY (`measurement_unit`)
    REFERENCES `eventspace`.`measurement_unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_charge_type1`
    FOREIGN KEY (`charge_type`)
    REFERENCES `eventspace`.`charge_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_space1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_space2`
    FOREIGN KEY (`link_space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reservation_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reservation_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `label` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `order_id` VARCHAR(20) NULL,
  `user` INT NOT NULL,
  `space` INT NOT NULL,
  `reservation_status` INT NOT NULL,
  `action_taker` INT NOT NULL,
  `event_type` INT NOT NULL,
  `guest_count` INT NULL DEFAULT 1,
  `seating_arrangement` INT NULL DEFAULT 1,
  `booking_charge` DOUBLE NULL,
  `refund` DOUBLE NULL DEFAULT NULL,
  `remarks` VARCHAR(450) NULL,
  INDEX `fk_user_has_space_space1_idx` (`space` ASC),
  INDEX `fk_user_has_space_user1_idx` (`user` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_booking_reservation_status1_idx` (`reservation_status` ASC),
  INDEX `fk_booking_event_type1_idx` (`event_type` ASC),
  CONSTRAINT `fk_user_has_space_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_space_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_event_type1`
    FOREIGN KEY (`event_type`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reviews` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rate` VARCHAR(10) NOT NULL,
  `title` VARCHAR(50) NULL,
  `description` VARCHAR(255) NULL,
  `created_at` DATETIME NOT NULL,
  `booking_id` INT NOT NULL,
  PRIMARY KEY (`id`, `booking_id`),
  INDEX `fk_reviews_booking1_idx` (`booking_id` ASC),
  UNIQUE INDEX `booking_id_UNIQUE` (`booking_id` ASC),
  CONSTRAINT `fk_reviews_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_amenity` (
  `space` INT NOT NULL,
  `amenity` INT NOT NULL,
  PRIMARY KEY (`space`, `amenity`),
  INDEX `fk_space_has_amenities_amenities1_idx` (`amenity` ASC),
  INDEX `fk_space_has_amenities_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_amenity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_amenity_amenities1`
    FOREIGN KEY (`amenity`)
    REFERENCES `eventspace`.`amenity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_extra_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_extra_amenity` (
  `space` INT NOT NULL,
  `amenity` INT NOT NULL,
  `amenity_unit` INT NOT NULL,
  `extra_rate` DECIMAL(8,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (`space`, `amenity`),
  INDEX `fk_space_has_amenities1_amenities1_idx` (`amenity` ASC),
  INDEX `fk_space_has_amenities1_space1_idx` (`space` ASC),
  INDEX `fk_space_has_extra_amenity_amenity_unit1_idx` (`amenity_unit` ASC),
  CONSTRAINT `fk_space_has_amenities1_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_amenities1_amenities1`
    FOREIGN KEY (`amenity`)
    REFERENCES `eventspace`.`amenity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_extra_amenity_amenity_unit1`
    FOREIGN KEY (`amenity_unit`)
    REFERENCES `eventspace`.`amenity_unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NOT NULL,
  `url` VARCHAR(450) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_image_space1_idx` (`space` ASC),
  CONSTRAINT `fk_image_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_event_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_event_type` (
  `space` INT NOT NULL,
  `event_type` INT NOT NULL,
  PRIMARY KEY (`space`, `event_type`),
  INDEX `fk_space_has_event_type_event_type1_idx` (`event_type` ASC),
  INDEX `fk_space_has_event_type_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_event_type_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_event_type_event_type1`
    FOREIGN KEY (`event_type`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_has_extra_amenity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_has_extra_amenity` (
  `booking` INT NOT NULL,
  `space_has_extra_amenity_space` INT NOT NULL,
  `space_has_extra_amenity_amenity` INT NOT NULL,
  `number` INT NOT NULL,
  `rate` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`booking`, `space_has_extra_amenity_space`, `space_has_extra_amenity_amenity`),
  INDEX `fk_booking_has_space_has_extra_amenity_space_has_extra_amen_idx` (`space_has_extra_amenity_space` ASC, `space_has_extra_amenity_amenity` ASC),
  INDEX `fk_booking_has_space_has_extra_amenity_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_booking_has_space_has_extra_amenity_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_has_space_has_extra_amenity_space_has_extra_amenity1`
    FOREIGN KEY (`space_has_extra_amenity_space` , `space_has_extra_amenity_amenity`)
    REFERENCES `eventspace`.`space_has_extra_amenity` (`space` , `amenity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_history` (
  `booking` INT NOT NULL,
  `reservation_status` INT NOT NULL,
  `refund_amount` DECIMAL(14,4) NULL,
  `action_taker` INT NOT NULL,
  `created_at` DATETIME NULL,
  `seen` TINYINT(1) NOT NULL DEFAULT 0,
  INDEX `fk_table1_booking1_idx` (`booking` ASC),
  INDEX `fk_table1_reservation_status1_idx` (`reservation_status` ASC),
  PRIMARY KEY (`booking`, `reservation_status`),
  INDEX `fk_booking_history_user1_idx` (`action_taker` ASC),
  CONSTRAINT `fk_table1_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_reservation_status1`
    FOREIGN KEY (`reservation_status`)
    REFERENCES `eventspace`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_history_user1`
    FOREIGN KEY (`action_taker`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`db_changes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`db_changes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `version` VARCHAR(45) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `version_UNIQUE` (`version` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`seating_arrangement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`seating_arrangement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `icon` VARCHAR(45) NULL,
  `mobile_icon` VARCHAR(250) NULL,
  `date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_unavailablity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_unavailablity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  `is_blocked` TINYINT(1) NOT NULL DEFAULT '1',
  `title` VARCHAR(245) NULL,
  `date_booking_made` VARCHAR(45) NULL,
  `guest_name` VARCHAR(245) NULL,
  `guest_contact_number` VARCHAR(45) NULL,
  `guest_email` VARCHAR(45) NULL,
  `no_of_guests` VARCHAR(445) NULL,
  `event_type_id` INT NULL,
  `extras_requested` VARCHAR(545) NULL,
  `seating_arrangement_id` INT NULL,
  `cost` VARCHAR(45) NULL,
  `note` VARCHAR(545) NULL,
  `is_manual` TINYINT(1) NULL,
  `remarks` VARCHAR(450) NULL,
  `reservation_status` INT NULL,
  `booking_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_space_unavailablity_space1_idx` (`space` ASC),
  INDEX `fk_space_unavailablity_event_type1_idx` (`event_type_id` ASC),
  INDEX `fk_space_unavailablity_seating_arrangement1_idx` (`seating_arrangement_id` ASC),
  CONSTRAINT `fk_space_unavailablity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_unavailablity_event_type1`
    FOREIGN KEY (`event_type_id`)
    REFERENCES `eventspace`.`event_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_unavailablity_seating_arrangement1`
    FOREIGN KEY (`seating_arrangement_id`)
    REFERENCES `eventspace`.`seating_arrangement` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`payment_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`payment_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking` INT NOT NULL,
  `response` INT NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_history_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_payment_history_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_expire_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_expire_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking` INT NOT NULL,
  `expire_date` DATETIME NOT NULL,
  `expired` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_booking_expire_details_booking1_idx` (`booking` ASC),
  CONSTRAINT `fk_booking_expire_details_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`search_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`search_address` (
  `space` INT NOT NULL,
  `address` VARCHAR(90) NOT NULL,
  INDEX `fk_search_address_space1_idx` (`space` ASC),
  UNIQUE INDEX `space_UNIQUE` (`space` ASC),
  PRIMARY KEY (`space`),
  CONSTRAINT `fk_search_address_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_seating_arrangement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_seating_arrangement` (
  `space` INT NOT NULL,
  `seating_arrangement` INT NOT NULL,
  `participant_count` INT NOT NULL,
  INDEX `fk_space_has_space1_idx` (`space` ASC),
  INDEX `fk_space_has_seating_arrangement_seating_arrangement1_idx` (`seating_arrangement` ASC),
  PRIMARY KEY (`space`, `seating_arrangement`),
  CONSTRAINT `fk_space_has_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_seating_arrangement_seating_arrangement1`
    FOREIGN KEY (`seating_arrangement`)
    REFERENCES `eventspace`.`seating_arrangement` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`rule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`rule` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(45) NULL,
  `icon` VARCHAR(45) NULL,
  `mobile_icon` VARCHAR(250) NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_rule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_rule` (
  `space` INT NOT NULL,
  `rule` INT NOT NULL,
  INDEX `fk_space_has_rule_space1_idx` (`space` ASC),
  INDEX `fk_space_has_rule_rules1_idx` (`rule` ASC),
  PRIMARY KEY (`space`, `rule`),
  CONSTRAINT `fk_space_has_rule_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_rule_rules1`
    FOREIGN KEY (`rule`)
    REFERENCES `eventspace`.`rule` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`auto_publish`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`auto_publish` (
  `id` INT NOT NULL,
  `enabled` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`day`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`day` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`block_availability`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`block_availability` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NOT NULL,
  `day` INT NOT NULL,
  `from_time` TIME NULL,
  `to_time` TIME NULL,
  `menu_items` VARCHAR(450) NULL,
  `charge` DOUBLE NULL,
  `active` TINYINT(1) NULL,
  `is_reimbursable` TINYINT(1) NULL,
  INDEX `fk_availablity_blocks_space1_idx` (`space` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_block_availability_day1_idx` (`day` ASC),
  CONSTRAINT `fk_availablity_blocks_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_block_availability_day1`
    FOREIGN KEY (`day`)
    REFERENCES `eventspace`.`day` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`user_additional_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`user_additional_details` (
  `user` INT NOT NULL,
  `last_name` VARCHAR(45) NULL,
  `address` VARCHAR(90) NULL,
  `job` VARCHAR(45) NULL,
  `commission_percentage` INT NULL,
  `company_name` VARCHAR(45) NULL,
  `about` VARCHAR(450) NULL,
  `account_holder_name` VARCHAR(45) NULL,
  `account_number` VARCHAR(45) NULL,
  `bank` VARCHAR(45) NULL,
  `bank_branch` VARCHAR(45) NULL,
  INDEX `fk_user_additional_details_user1_idx` (`user` ASC),
  CONSTRAINT `fk_user_additional_details_user1`
    FOREIGN KEY (`user`)
    REFERENCES `eventspace`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`per_hour_availablity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`per_hour_availablity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NOT NULL,
  `day` INT NOT NULL,
  `from_time` TIME NOT NULL,
  `to_time` TIME NOT NULL,
  `charge` DOUBLE NOT NULL,
  `active` SMALLINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_per_hour_availablity_space1_idx` (`space` ASC),
  INDEX `fk_per_hour_availablity_day1_idx` (`day` ASC),
  CONSTRAINT `fk_per_hour_availablity_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_per_hour_availablity_day1`
    FOREIGN KEY (`day`)
    REFERENCES `eventspace`.`day` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_slots`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_slots` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking` INT NOT NULL,
  `from_date` DATETIME NOT NULL,
  `to_date` DATETIME NOT NULL,
  INDEX `fk_booking_slots_booking1_idx` (`booking` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_booking_slots_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`featured_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`featured_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `rate` DOUBLE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`featured_spaces`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`featured_spaces` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `space` INT NULL,
  `featured_type` INT NOT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  `featured` TINYINT(1) NOT NULL,
  INDEX `fk_featured_spaces_space1_idx` (`space` ASC),
  INDEX `fk_featured_spaces_featured_type1_idx` (`featured_type` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_featured_spaces_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_featured_spaces_featured_type1`
    FOREIGN KEY (`featured_type`)
    REFERENCES `eventspace`.`featured_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`menu_files`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`menu_files` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `menu_id` VARCHAR(45) NULL,
  `url` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_menu_files`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_menu_files` (
  `space` INT NOT NULL,
  `menu_files` INT NOT NULL,
  PRIMARY KEY (`space`, `menu_files`),
  INDEX `fk_space_has_menu_files_menu_files1_idx` (`menu_files` ASC),
  INDEX `fk_space_has_menu_files_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_menu_files_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_menu_files_menu_files1`
    FOREIGN KEY (`menu_files`)
    REFERENCES `eventspace`.`menu_files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`block_availability_has_menu_files`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`block_availability_has_menu_files` (
  `block_availability` INT NOT NULL,
  `menu_files` INT NOT NULL,
  PRIMARY KEY (`block_availability`, `menu_files`),
  INDEX `fk_block_availability_has_menu_files_menu_files1_idx` (`menu_files` ASC),
  INDEX `fk_block_availability_has_menu_files_block_availability1_idx` (`block_availability` ASC),
  CONSTRAINT `fk_block_availability_has_menu_files_block_availability1`
    FOREIGN KEY (`block_availability`)
    REFERENCES `eventspace`.`block_availability` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_block_availability_has_menu_files_menu_files1`
    FOREIGN KEY (`menu_files`)
    REFERENCES `eventspace`.`menu_files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`reimbursable_options`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`reimbursable_options` (
  `id` INT NOT NULL,
  `code` VARCHAR(45) NULL,
  `desc` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`block_charge_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`block_charge_type` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_additional_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_additional_details` (
  `space` INT NOT NULL,
  `contact_person_name` VARCHAR(45) NULL,
  `mobile_phone` VARCHAR(45) NULL,
  `company_phone` VARCHAR(45) NULL,
  `contact_person_name2` VARCHAR(45) NULL,
  `mobile_phone2` VARCHAR(12) NULL,
  `company_phone2` VARCHAR(12) NULL,
  `host_logo` VARCHAR(250) NULL,
  `account_holder_name` VARCHAR(145) NULL,
  `account_number` VARCHAR(45) NULL,
  `bank` VARCHAR(45) NULL,
  `bank_branch` VARCHAR(45) NULL,
  `commission_percentage` VARCHAR(45) NULL,
  `company_name` VARCHAR(145) NULL,
  INDEX `fk_table1_space1_idx` (`space` ASC),
  CONSTRAINT `fk_table1_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`promo_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`promo_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `to_all_spaces` INT NOT NULL,
  `promo_code` VARCHAR(7) NOT NULL,
  `discount` INT NOT NULL,
  `is_discount_flat` INT NOT NULL DEFAULT 0,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `create_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `promo_code_UNIQUE` (`promo_code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`booking_has_promotion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`booking_has_promotion` (
  `booking` INT NOT NULL,
  `promo_details` INT NOT NULL,
  INDEX `fk_booking_has_promotion_booking1_idx` (`booking` ASC),
  INDEX `fk_booking_has_promotion_promo_details1_idx` (`promo_details` ASC),
  CONSTRAINT `fk_booking_has_promotion_booking1`
    FOREIGN KEY (`booking`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_has_promotion_promo_details1`
    FOREIGN KEY (`promo_details`)
    REFERENCES `eventspace`.`promo_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`ios_release`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`ios_release` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `version` VARCHAR(45) NULL,
  `created_at` DATETIME NULL DEFAULT now(),
  `update_required` INT NULL,
  `release_note` VARCHAR(250) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`android_release`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`android_release` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `version` VARCHAR(45) NULL,
  `created_at` DATETIME NULL DEFAULT now(),
  `update_required` INT NULL,
  `release_note` VARCHAR(250) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`schedule_start`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`schedule_start` (
  `id` INT NOT NULL,
  `is_started` TINYINT(2) NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`payment_verify`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`payment_verify` (
  `booking_id` INT NOT NULL,
  `verified` INT NOT NULL DEFAULT 0,
  INDEX `fk_payment_verify_booking1_idx` (`booking_id` ASC),
  CONSTRAINT `fk_payment_verify_booking1`
    FOREIGN KEY (`booking_id`)
    REFERENCES `eventspace`.`booking` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_type` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `icon` VARCHAR(45) NULL,
  `mobile_icon` VARCHAR(45) NULL,
  `date_updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`space_has_space_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`space_has_space_type` (
  `space` INT NOT NULL,
  `space_type` INT NOT NULL,
  PRIMARY KEY (`space`, `space_type`),
  INDEX `fk_space_has_space_type_space_type1_idx` (`space_type` ASC),
  INDEX `fk_space_has_space_type_space1_idx` (`space` ASC),
  CONSTRAINT `fk_space_has_space_type_space1`
    FOREIGN KEY (`space`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_space_has_space_type_space_type1`
    FOREIGN KEY (`space_type`)
    REFERENCES `eventspace`.`space_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eventspace`.`promo_details_has_space`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eventspace`.`promo_details_has_space` (
  `promo_details_id` INT NOT NULL,
  `space_id` INT NOT NULL,
  PRIMARY KEY (`promo_details_id`, `space_id`),
  INDEX `fk_promo_details_has_space_space1_idx` (`space_id` ASC),
  INDEX `fk_promo_details_has_space_promo_details1_idx` (`promo_details_id` ASC),
  CONSTRAINT `fk_promo_details_has_space_promo_details1`
    FOREIGN KEY (`promo_details_id`)
    REFERENCES `eventspace`.`promo_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_promo_details_has_space_space1`
    FOREIGN KEY (`space_id`)
    REFERENCES `eventspace`.`space` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `eventspace` ;

-- -----------------------------------------------------
-- procedure deleteSpace
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteSpace`(in  p_spaceId int(11))
begin
declare returnVal Boolean default 0;
IF NOT EXISTS (select * from booking where space=p_spaceId) THEN
	BEGIN
		delete image,space_has_extra_amenity,space_has_amenity,space_has_event_type
		from space
		join image
			on space.id=image.space
		join space_has_amenity
			on space.id=space_has_amenity.space
		join space_has_event_type
			on space.id=space_has_event_type.space
		join space_has_extra_amenity
			on space.id=space_has_extra_amenity.space
		where space.id=p_spaceId;
		
		DELETE space FROM space 
WHERE
    space.id = p_spaceId;
        set returnVal = 1;
	END;

end if;
SELECT returnVal;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure filterSpace
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `filterSpace`(in params1 int,in params2 int,in rate1 double,in rate2 double,in amenityList TEXT,in eventList TEXT, in limitFor int)
BEGIN
		select dataset2.*,dataset1.count_event, dataset1.count_amenity from
			(
			(select x.space as id, COALESCE(x.count_event, 0) as count_event, COALESCE(y.count_amenity, 0) as count_amenity  from
				(select space, count(event_type) as count_event from space_has_event_type where   FIND_IN_SET (event_type, eventList) group by space) as x
						left join
				(select space, count(amenity) as count_amenity from space_has_amenity where  FIND_IN_SET (amenity, amenityList) group by space) as y
						on x.space = y.space
			)
		union
			(select y.space as id, COALESCE(x.count_event, 0) as count_event, COALESCE(y.count_amenity, 0) as count_amenity from
				(select space, count(event_type) as count_event from space_has_event_type where  FIND_IN_SET (event_type, eventList) group by space) as x
						right join
				(select space, count(amenity) as count_amenity from space_has_amenity where  FIND_IN_SET (amenity, amenityList) group by space) as y
						on x.space = y.space
			)
				  ) as dataset1
		right join
			(select * from space WHERE participant_count>= params1 and participant_count<= params2 and rate_per_hour>= rate1 and rate_per_hour<= rate2 and  approved= 1) as dataset2
		on  dataset1.id = dataset2.id 	group by dataset2.id
		order by  dataset1.count_event desc, dataset1.count_amenity desc limit limitFor;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure averageRating
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `averageRating`(in spaceId int)
BEGIN
	SELECT round(avg(SUBSTRING_INDEX(r.rate,'|',1)),2) as average 
    FROM booking b, reviews r, space s 
    where b.id=r.booking_id 
    and s.id=b.space 
    and s.id=spaceId;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure spaceByCoordinates
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spaceByCoordinates`(in latitudeVal varchar(50),in longitudeVal varchar(50),in radius int)
BEGIN
	SELECT * 
    from space 
    where approved=1 and ( 6371 * acos( cos( radians(latitudeVal) ) * 
    cos( radians( latitude ) ) * 
    cos( radians( longitude ) - radians(longitudeVal) ) + 
    sin( radians(latitudeVal) ) * 
    sin( radians( latitude ) ) ) )  < radius;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure bookingCharge
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `bookingCharge`(in bookingId int)
BEGIN
	SELECT (round(TIMESTAMPDIFF(second, booking.from_date, booking.to_date)/3600,2)
				*space.rate_per_hour+
				COALESCE(sum(booking_has_extra_amenity.number*booking_has_extra_amenity.rate),0))*1
				FROM booking 
				join space 
				on booking.space=space.id 
				left join booking_has_extra_amenity 
				on booking.id=booking_has_extra_amenity.booking
				where booking.id=bookingId;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure isCancelledAfterPaid
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `isCancelledAfterPaid`(in booking_id int)
BEGIN
	DECLARE b_id int(11) DEFAULT null;
    DECLARE b_status int(11) DEFAULT null;
    DECLARE is_cancelled_after_paid boolean default false;
    DECLARE v_finished INTEGER DEFAULT 0;
	DECLARE cursor_name CURSOR FOR select booking,reservation_status from booking_history where booking=booking_id;
    
     -- declare NOT FOUND handler
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;
    IF EXISTS (SELECT booking from booking_history where booking=booking_id and reservation_status=4) THEN
		BEGIN
			OPEN cursor_name;
			get_list: LOOP
				FETCH cursor_name INTO b_id,b_status;
				IF v_finished = 1 THEN 
					LEAVE get_list;
				END IF;
				
				IF
					b_status=3
					THEN
						SET is_cancelled_after_paid=TRUE;
						SELECT is_cancelled_after_paid;
						LEAVE get_list;
				END IF;
			END LOOP get_list;
            SELECT is_cancelled_after_paid;
		END;
	END IF;
    SELECT is_cancelled_after_paid;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure getCancelledDate
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getCancelledDate`(in bookingId int, in bookingStatus int)
BEGIN
	select created_at from booking_history where booking=bookingId and reservation_status=bookingStatus;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure forceDeleteSpace
-- -----------------------------------------------------

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `forceDeleteSpace`(in  p_spaceId int(11))
begin
declare returnVal Boolean default 0;
IF EXISTS (select * from booking where space=p_spaceId) THEN
	BEGIN
		delete booking_has_extra_amenity
			from booking_has_extra_amenity
			where space_has_extra_amenity_space=p_spaceId;
        
       delete booking_history, reviews
        from booking
			left join booking_history
				on booking.id=booking_history.booking
			left join reviews
				on booking.id=reviews.booking_id
        where booking.space=p_spaceId;
        
        delete booking
			from booking
			where booking.space=p_spaceId;
       
	end;
    end if;
	delete image,space_has_amenity,space_has_event_type,space_has_extra_amenity
	from space
	left join image
		on space.id=image.space
	left join space_has_amenity
		on space.id=space_has_amenity.space
	left join space_has_event_type
		on space.id=space_has_event_type.space
	left join space_has_extra_amenity
		on space.id= space_has_extra_amenity.space
	where space.id=p_spaceId;
	
	delete space from space where space.id=p_spaceId;
	set returnVal = 1;
select returnVal;
end$$

DELIMITER ;
USE `eventspace`;

DELIMITER $$
USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`space_BEFORE_INSERT` BEFORE INSERT ON `space` FOR EACH ROW
BEGIN
    declare approve int;
    if exists(select enabled from auto_publish where id =1) 
		then
		set approve= (select enabled from auto_publish where id =1);
		SET NEW.approved = approve;
	end if;
END$$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`space_BEFORE_UPDATE` BEFORE UPDATE ON `space` FOR EACH ROW
BEGIN
 if NEW.approved=1 then
		if OLD.approved_date IS NULL then
		SET NEW.approved_date = now();
        end if;
    end if;
END$$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_INSERT` AFTER INSERT ON `booking` FOR EACH ROW
BEGIN
	INSERT INTO `eventspace`.`booking_history`
	VALUES(New.id,New.reservation_status,null,New.action_taker,now(),0);
    
END$$

USE `eventspace`$$
CREATE DEFINER = CURRENT_USER TRIGGER `eventspace`.`booking_AFTER_UPDATE` AFTER UPDATE ON `booking` FOR EACH ROW
BEGIN

	IF NOT(New.reservation_status=OLD.reservation_status) then
    
	#if manual BOOKING
	IF New.reservation_status = 2 then
        if OLD.reservation_status =1 then
        	
		INSERT INTO `eventspace`.`booking_expire_details` (`booking`,`expire_date`) 
		VALUES (new.id,DATE_ADD(now(), INTERVAL 1 day));
            
		end if;
	END IF;

	#if manual pay
	IF New.reservation_status = 3 then
        if OLD.reservation_status =2 then
        
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
            
		end if;
	END IF;
    
		if New.reservation_status = 4 or New.reservation_status = 6 then
			UPDATE `eventspace`.`booking_expire_details`
			SET expired=1
			WHERE(booking= New.id);
		end if;
		
		
		#if undo pay
		IF New.reservation_status = 2 then
			IF OLD.reservation_status =3 then
				UPDATE `eventspace`.`booking_expire_details`
				SET expired=0
				WHERE(booking= New.id);
            
				DELETE FROM `eventspace`.`booking_history` 
				WHERE  booking=new.id and reservation_status=OLD.reservation_status;
			END IF;
			IF OLD.reservation_status =1 then	
				INSERT INTO `eventspace`.`booking_history`
				VALUES(New.id, New.reservation_status, NULL, New.action_taker, now(), 0);
				END IF;
        END IF;
		
		
			IF NOT (New.reservation_status = 2)  then
				INSERT INTO `eventspace`.`booking_history`
				VALUES(New.id, New.reservation_status, New.refund, New.action_taker, now(), 0);
			END IF;
	END IF;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
